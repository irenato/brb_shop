<?php
/**
 * Created by PhpStorm.
 * User: Khalimov Renato
 * Date: 12.11.2016
 * Time: 10:14
 */


get_header();
?>

<?php if (have_posts()) : while (have_posts()) :
    the_post(); ?>

    <div class="container fatherSon-container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="main-blocks about-us col-md-12">
                    <h2 class="main-blocks__title"><?= get_the_title(); ?></h2>
                    <?php $image = get_field('action_image');
                    $url_for_iframe = get_field('you_tube_link')
                    ?>
                    <?php if ($image || $url_for_iframe): ?>
                        <div class="promo-fatherSon">
                            <?php if ($image): ?>
                                <img src="<?= $image; ?>">
                            <?php endif; ?>
                            <p><?= get_the_content(); ?></p>
                            <?php if ($url_for_iframe): ?>
                                <div class="emb-responsive">
                                    <iframe width="100%" height="480" src="<?= $url_for_iframe ?>"
                                            frameborder="0" allowfullscreen></iframe>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container commets-container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="main-blocks about-us col-md-12">
                    <h2 class="main-blocks__title">отзывы</h2>
                    <div class="reviewsN">
                        <?php
                        global $post;
                        woocommerce_view_comments_for_action($post);
                        ?>
                        <!--                        <article class="rewiwesNew">-->
                        <!--                            <footer>-->
                        <!--                                <p>Андрей</p>-->
                        <!--                            </footer>-->
                        <!--                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pretium ac orci sed-->
                        <!--                                feugiat.-->
                        <!--                                Cusociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>-->
                        <!--                            <p class="date-comments">-->
                        <!--                                <time datetime="2016-02-15">15/02/2016</time>-->
                        <!--                            </p>-->
                        <!--                        </article>-->

                        <!--                        <article class="rewiwesNew">-->
                        <!--                            <footer>-->
                        <!--                                <p>Андрей</p>-->
                        <!--                            </footer>-->
                        <!--                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pretium ac orci sed-->
                        <!--                                feugiat.-->
                        <!--                                Cusociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>-->
                        <!--                            <p class="date-comments">-->
                        <!--                                <time datetime="2016-02-15">15/02/2016</time>-->
                        <!--                            </p>-->
                        <!--                        </article>-->
                        <!--                        <article class="rewiwesNew">-->
                        <!--                            <footer>-->
                        <!--                                <p>Андрей</p>-->
                        <!--                            </footer>-->
                        <!--                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pretium ac orci sed-->
                        <!--                                feugiat.-->
                        <!--                                Cusociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>-->
                        <!--                            <p class="date-comments">-->
                        <!--                                <time datetime="2016-02-15">15/02/2016</time>-->
                        <!--                            </p>-->
                        <!--                        </article>-->
                    </div>
                    <div class="comments">
                        <?php woocommerce_view_form_for_action(); ?>
                        <!--                        <form>-->
                        <!--                            <textarea name="new-comments" placeholder="Написать отзыв"></textarea>-->
                        <!--                            <button type="submit">Отправить</button>-->
                        <!--                        </form>-->
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
