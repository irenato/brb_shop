<?php

// переключение на выбор филиала
session_start();

if (!isset($_SESSION['branches'])) {
    wp_redirect(home_url() . '/ru/branches');
    $_SESSION['branches'] = true;
}

// Проверяем, какой филиал выбран
if(isset($_GET['branch']) && ($_GET['branch'] == 2705 || $_GET['branch'] == 2706 || $_GET['branch'] == 2708)){
    $branch = $_GET['branch'];
    $_SESSION['branch'] = $branch;
}else{
    $branch = isset($_SESSION['branch'])? $_SESSION['branch'] : 2705;
}

// Переменная хранит номер филиала
define('BRANCH', $branch);
define('THEMROOT', get_stylesheet_directory_uri());

// подключение меню
add_action('after_setup_theme', function () {
    register_nav_menus(array(
        'header_menu' => 'Меню в шапке'
    ));
});
// подключение меню конец
add_theme_support('post-thumbnails');

if (function_exists('register_sidebar'))
    register_sidebar(array(
        'name' => 'Боковая колонка интернет магазина',
        'id' => 'shop_sidebar',
        'before_widget' => '<li>',
        'after_widget' => '</li>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));

// Подключение настроек темы
require get_template_directory() . '/colt_theme_settings/my_scripts.php';
require get_template_directory() . '/colt_theme_settings/colt_theme_settings.php';
require get_template_directory() . '/colt_theme_settings/class_p2p.php';
require get_template_directory() . '/colt_theme_settings/remove_menu_items.php';
require get_template_directory() . '/colt_theme_settings/other_post_types.php';
require get_template_directory() . '/colt_theme_settings/my_ajax_functions.php';