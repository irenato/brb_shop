<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="cmsmagazine" content="58666b727c9a8fb51fe902c422da78cb"/>
    <?php global $page_id;
    $page_id = $post->ID ?>
    <title><?php if ($post->ID == 51) echo 'Главная'; else wp_title('|', true, 'right'); ?></title>
    <?php
    global $wp_query;
    global $post;
    global $options;
    $options = tf_variable_exist(get_option('theme_settings'));


    ?>
    <?php if (tf_variable_exist($options['favicon'])): ?>
        <?php $favicon_url = wp_get_attachment_image_url($options['favicon'], 'full'); ?>
        <link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo esc_url($favicon_url); ?>">
    <?php endif; ?>

    <!-- style start -->
    <?php
    $theme_path_uri = get_template_directory_uri();
    ?>
    <link rel="stylesheet" href="<?php echo esc_url($theme_path_uri); ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo esc_url($theme_path_uri); ?>/css/temp.css">
    <link rel="stylesheet" href="<?php echo esc_url($theme_path_uri); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo esc_url($theme_path_uri); ?>/css/lightslider.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- style end -->
    <script src="<?php echo esc_url($theme_path_uri); ?>/js/jquery.min.js"></script>
    <script src="<?php echo esc_url($theme_path_uri); ?>/js/lightslider.js"></script>
    <?php wp_head(); ?>
    <?php
    /**
     * Хук предназначен для Google аналитики
     */
    do_action('before_head_close');
    ?>
    <!--GoogleAnalyticsObject-->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-74992501-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>

<?php
/**
 * Хук предназначен для Яндекс метрики
 */
do_action('before_head_close');
?>
<header class="header cbp-af-header hidden-md">
    <div class="container cbp-af-inner">
        <div class="header-box clearfix">
            <div class="header-search pull-left">
                <div class="search__form pull-left">
                    <?php get_search_form(); ?>
                </div>
                <nav class="nav__social pull-left">
                    <ul class="nav__social-list" style="display: inline-block;">
                        <?php
                        $social_vk_visible = isset($options['social_vk_visible']) ? $options['social_vk_visible'] : '';
                        $social_vk_url = isset($options['social_vk_url']) ? $options['social_vk_url'] : '';
                        $social_facebook_visible = isset($options['social_facebook_visible']) ? $options['social_facebook_visible'] : '';
                        $social_facebook_url = isset($options['social_facebook_url']) ? $options['social_facebook_url'] : '';
                        $social_instagram_visible = isset($options['social_instagram_visible']) ? $options['social_instagram_visible'] : '';
                        $social_instagram_url = isset($options['social_instagram_url']) ? $options['social_instagram_url'] : '';
                        $social_twitter_visible = isset($options['social_twitter_visible']) ? $options['social_twitter_visible'] : '';
                        $social_twitter_url = isset($options['social_twitter_url']) ? $options['social_twitter_url'] : '';
                        $social_youtube_url = isset($options['social_youtube_url']) ? $options['social_youtube_url'] : '';
                        $social_youtube_visible = isset($options['social_youtube_visible']) ? $options['social_youtube_visible'] : '';
                        $social_pinterest_url = isset($options['social_pinterest_url']) ? $options['social_pinterest_url'] : '';
                        $social_pinterest_visible = isset($options['social_pinterest_visible']) ? $options['social_pinterest_visible'] : '';
                        $social_periscope_url = isset($options['social_periscope_url']) ? $options['social_periscope_url'] : '';
                        $social_periscope_visible = isset($options['social_periscope_visible']) ? $options['social_periscope_visible'] : '';
                        ?>
                        <?php if ($social_vk_visible && $social_vk_visible == '1' &&
                            $social_vk_url != ''
                        ): ?>
                            <li><a class="fa fa-vk" target="_blank" href="<?php echo esc_url($social_vk_url); ?>"></a>
                            </li>
                        <?php endif; ?>
                        <?php if ($social_facebook_visible && $social_facebook_visible == '1' &&
                            $social_facebook_url != ''
                        ): ?>
                            <li><a class="fa fa-facebook" target="_blank"
                                   href="<?php echo esc_url($social_facebook_url); ?>"></a></li>
                        <?php endif; ?>
                        <?php if ($social_instagram_visible && $social_instagram_visible == '1' &&
                            $social_instagram_url != ''
                        ): ?>
                            <li><a class="fa fa-instagram" target="_blank"
                                   href="<?php echo esc_url($social_instagram_url); ?>"></a></li>
                        <?php endif; ?>
                        <?php if ($social_twitter_visible && $social_twitter_visible == '1' &&
                            $social_twitter_url != ''
                        ): ?>
                            <li><a class="fa fa-twitter" target="_blank"
                                   href="<?php echo esc_url($social_twitter_url); ?>"></a></li>
                        <?php endif; ?>
                        <?php if ($social_youtube_visible && $social_youtube_visible == '1' &&
                            $social_youtube_url != ''
                        ): ?>
                            <li><a class="fa fa-youtube" target="_blank"
                                   href="<?php echo esc_url($social_youtube_url); ?>"></a></li>
                        <?php endif; ?>
                        <?php if ($social_pinterest_visible && $social_pinterest_visible == '1' &&
                            $social_pinterest_url != ''
                        ): ?>
                            <li><a class="fa fa-pinterest" target="_blank"
                                   href="<?php echo esc_url($social_pinterest_url); ?>"></a></li>
                        <?php endif; ?>
                        <?php if ($social_periscope_visible && $social_periscope_visible == '1' &&
                            $social_periscope_url != ''
                        ): ?>
                            <li><a class="icon-periscope-2" target="_blank"
                                   href="<?php echo esc_url($social_periscope_url); ?>"></a></li>
                        <?php endif; ?>
                    </ul>
                </nav>
            </div>
            <div class="working-hours pull-right">
                <a class="header-check pull-right" href="<?= get_home_url() . '/branches' ?>">Выбор филиала</a>
                <div class="pull-right working-times">
                    <?php if (tf_variable_exist($options['contacts_schedule'])): ?>
                        <p class="schedule"><?= get_field('schedule', BRANCH) ?></p>
                    <?php endif; ?>
                    <?php $phones = get_field('phones', BRANCH);
                    $i = 0; ?>
                    <p class="check-by-phone">Запись по телефону:
                        <?php if ($phones) : ?>
                            <?php foreach ($phones as $phone) :
                                ++$i; ?>
                                <?= esc_html($phone['phone']); ?>
                                <?= $i < count($phones) ? ', ' : '' ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="header-navbarHL clearfix">
            <ul class="pull-left">
                <li>
                    <!-- ШКОЛА БАРБЕРОВ И ТАТУ -->
                    <?php $post_id_22 = get_post(22); ?>
                    <a href="<?= $post_id_22->guid ?>"><?= $post_id_22->post_title ?></a>
                </li>
                <li>
                    <!-- МАГАЗИН -->
                    <?php $post_id_26 = get_post(26); ?>
                    <a href="<?= $post_id_26->guid ?>"><?= $post_id_26->post_title ?></a>
                </li>
                <li>
                    <!-- ФРАНШИЗА -->
                    <?php $post_id_28 = get_post(28); ?>
                    <a href="<?= $post_id_28->guid ?>"><?= $post_id_28->post_title ?></a>
                </li>
            </ul>
            <ul class="pull-right">
                <li>
                    <!-- Подарочные сертификаты -->
                    <?php $post_id_2718 = get_post(2718); ?>
                    <a href="<?= $post_id_2718->guid ?>"><?= $post_id_2718->post_title ?></a>
                </li>
                <li>
                    <!-- СМИ О НАС -->
                    <?php $post_id_30 = get_post(30); ?>
                    <a href="<?= $post_id_30->guid ?>"><?= $post_id_30->post_title ?></a>
                </li>
                <li>
                    <!-- Блог -->
                    <?php $post_id_24 = get_post(24); ?>
                    <a href="<?= $post_id_24->guid ?>"><?= $post_id_24->post_title ?></a>
                </li>
            </ul>
        </div>
        <div class="header-navbarFL clearfix">
            <nav class="header-nav__left pull-left">
                <ul class="header-nav">
                    <li>
                        <?php $post_id_2802= get_post(2802); ?>
                        <a href="<?= $post_id_2802->guid ?>">О нас</a>
                    </li>
                    <li>
                        <a href="http://mrcolt.com.ua/ru/barber-mastera/">Барбер мастера</a>
                    </li>
                    <li>
                        <a href="http://mrcolt.com.ua/ru/tatu-mastera/">Тату мастера</a>
                    </li>
                </ul>
            </nav>
            <nav class="header-nav__right pull-right">
                <ul class="header-nav">
                    <li>
                        <!-- Услуги -->
                        <?php $post_id_18 = get_post(2754); ?>
                        <?php $services_slugs = filter_services_id_by_filial(); ?>
                        <a href="<?= $post_id_18->guid ?>"><?= $post_id_18->post_title ?></a>
                        <ul class="dropdownL">

                            <?php for ($i = 1; $i < 11; $i++): ?>
                                <?php if (isset($options['block_services_' . $i . '_title']) && in_array(create_slug_from_services_url($options['block_services_' . $i . '_url']), $services_slugs)): ?>
                                    <li>
                                        <a href="<?= $options['block_services_' . $i . '_url'] ?>"><?= $options['block_services_' . $i . '_title'] ?></a>
                                    </li>
                                <?php endif; ?>
                            <?php endfor; ?>
                        </ul>
                    </li>
                    <li>
                        <!-- Акции -->
                        <?php $post_id_2713 = get_post(2713); ?>

                        <a href="<?= $post_id_2713->guid ?>"><?= $post_id_2713->post_title ?></a>
                        <ul class="dropdownL">

                            <?php $actions = PTP::getActions(); ?>
                            <?php if ($actions) : ?>
                                <?php foreach ($actions as $action) : ?>
                                    <?php $post_action = get_post($action->p2p_from) ?>

                                    <li>
                                        <a href="<?= $post_action->guid ?>"><?= $post_action->post_title ?></a>
                                    </li>

                                <?php endforeach; ?>
                            <?php endif; ?>

                        </ul>
                    </li>
                    <li>
                        <!-- Контакты -->
                        <?php $post_id_20 = get_post(20); ?>
                        <a href="<?= get_home_url() . '#contacts' ?>"><?= $post_id_20->post_title ?></a>
                    </li>
                </ul>
            </nav>
        </div>
        <?php
        $logo_url = $theme_path_uri . '/img/default_logo.png';
        $logo_uri = isset($options['logo_uri']) ? $options['logo_uri'] : '';

        if ($logo_uri) {
            $logo_url = wp_get_attachment_image_url($logo_uri, 300, 300);
        }
        ?>

        <div class="header-logo  hidden-md hidden-xs">
            <a href="<?= get_home_url(); ?>"><img src="<?= esc_url($logo_url); ?>" height="222" width="222" alt="logo"
                                                  title="<?= bloginfo('name') ?>"></a>
        </div>
        <div class="header-nav-bar-right hidden-md hidden-xs">
        </div>
    </div>
</header>
<div class="responsive__menu">
    <div class="container">
        <!--responsive menu-->
        <nav class="header-nav hidden-lg visible-xs visible-md">
            <input type="checkbox" id="hmt" class="hidden-menu-ticker">
            <label class="btn-menu" for="hmt">
                <span class="first"></span>
                <span class="second"></span>
                <span class="third"></span>
            </label>
            <ul class="hidden-menu">
                <?php if ($options['menu_left_side']): ?>
                    <?php
                    $menu_items = wp_get_nav_menu_items($options['menu_left_side']);
                    $current_page_url = get_permalink();

                    if ($menu_items):
                        foreach ($menu_items as $item):
                            $active = "";
                            if (strcmp($item->url, $current_page_url) == 0) {
                                $active = 'class="active"';
                            }
                            ?>
                            <li>
                                <a <?php echo $active; ?>
                                    href="<?php echo esc_url($item->url); ?>"><?php echo esc_html($item->title); ?></a>
                            </li>
                        <?php endforeach; ?>
                        <?php
                    endif;
                endif;
                ?>
                <?php if ($options['menu_left_side']): ?>
                    <?php
                    $menu_items = wp_get_nav_menu_items($options['menu_right_side']);
                    $current_page_url = get_permalink();

                    if ($menu_items):
                        foreach ($menu_items as $item):
                            $active = "";
                            if (strcmp($item->url, $current_page_url) == 0) {
                                $active = 'class="active"';
                            }
                            ?>
                            <li>
                                <a <?php echo $active; ?>
                                    href="<?php echo esc_url($item->url); ?>"><?php echo esc_html($item->title); ?></a>
                            </li>
                        <?php endforeach; ?>
                        <?php
                    endif;
                endif;
                ?>
            </ul>
        </nav>
    </div>
</div>

<?php

if ($page_id == 51) : ?>
    <?php //if (is_front_page()): ?>
    <section class="slider">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->

            <?php
            if (function_exists('slider_view')) {
                $args = array(
                    'before_img' => '<div class="item">',
                    'after_img' => '</div>',
                    'container_start' => '<div class="carousel-inner" role="listbox">',
                    'container_end' => '</div>',
                    'slider_slug' => 'slajder-glavnoj-stranicy'
                );
                $post_args = array(
                    'connected_type' => 'sliders_to_filial',
                    'connected_items' => BRANCH,
                    'nopaging' => true,
                );

                echo slider_view($args, $post_args);
            }
            ?>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <a href="#serviceOfUs" class="go-to-down">
<!--            <span class="go-to-down__nav glyphicon glyphicon-menu-down" aria-hidden="true"></span>-->
            <span class="go-to-down__nav glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        </a>
    </section>
<?php endif; ?>
<a href="#" class="buttonOnline ms_booking">Онлайн запись</a>
<section class="main"
         style="background: url(<?= get_field('background', BRANCH) ?>) repeat  50% 50%; background-size: 100%">
    <div class="container ">

        <?php if (!is_front_page() && ($post->post_type != 'product')): ?>
            <div class="breadcrumb" style="background-color: #dfdfe2;">
                <?php echo breadcrumbs(' » '); ?>
            </div>
        <?php endif; ?>

        <?php if ($post->post_type == 'product'): ?>
            <div class="breadcrumb" style="background-color: #dfdfe2;">
                <?php
                $args = array(
                    'delimiter' => '»'
                );
                echo woocommerce_breadcrumb($args);
                ?>
            </div>
        <?php endif; ?>

    </div>