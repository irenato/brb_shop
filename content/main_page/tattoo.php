<?php
/**
 * User: Максим Руденко
 * Date: 01.03.2016
 */
global $curr_lang;
$options = get_option('theme_settings');
$p2p = PTP::getTattoMasters();
if (count($p2p) > 0):
    ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="main-blocks about-us col-md-12">
                    <h2 class="main-blocks__title"><?php echo $options['tattoo_master_block_title' . $curr_lang] ? $options['tattoo_master_block_title' . $curr_lang] : ''; ?></h2>
                    <div class="tatoo-slider">
                        <ul class="<?= (count($p2p) >= 4) ? 'lightSlider' : 'sliderNewBox' ?> card-box">

                            <?php foreach ($p2p as $tatto): ?>
                                <?php $post = get_post($tatto->p2p_from); ?>
                                <?php $master_img_preview = tf_variable_exist(get_post_meta($tatto->p2p_from, '_master_img_preview', true)); ?>
                                <?php $img = wp_get_attachment_image_url($master_img_preview, 'master-preview'); ?>
                                <li>
                                    <div class="card-box__wrapp">
                                        <img src="<?= $img ?>">
                                        <div class="bottom-menu">
                                            <h4 class="bottom-menu__name"><?= $post->post_title ?></h4>
                                            <p class="bottom-menu__name-desc"><?= $options['barber_master_single_type' . $curr_lang] ? $options['barber_master_single_type' . $curr_lang] : "" ?></p>
                                            <a class="bottom-menu__link"
                                               href="<?= $post->guid ?>"><?= $options['master_view_button' . $curr_lang] ? $options['master_view_button' . $curr_lang] : "" ?></a>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>

                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="hidden-sm hidden-xs separator-line"></div>
<?php endif; ?>
