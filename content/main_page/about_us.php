<?php
/**
 * User: Максим Руденко
 * Date: 28.02.2016
 */
global $curr_lang;
$theme_path_uri = get_template_directory_uri();
$options = get_option('theme_settings');
$stories = get_posts(array(
    'connected_type' => 'about_us_to_filial',
    'connected_items' => BRANCH,
    'nopaging' => true,
));

?>

<?php if ($stories): ?>
<div id="about_us">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="main-blocks about-us col-md-12">

                    <h2 class="main-blocks__title"><?php echo esc_attr($options['block_mp_title' . $curr_lang] ? $options['block_mp_title' . $curr_lang] : ''); ?></h2>
                    <div class="col-md-6 main-blocks-left">
                        <div class="link-box autobiography"
                             style="background-image:url(<?= get_the_post_thumbnail_url($stories[0]->ID) ?>);">
                            <h3 class="link-box__title autobiography__title">
                                <?= $stories[0]->post_title ?>
                            </h3>
                            <div class="overlay">
                                <a href="<?= $stories[0]->guid ?>">
                                    <img class="overlay-img"
                                         src="<?php echo $theme_path_uri; ?>/img/main-links/logo-link.png">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 main-blocks-right">

                        <div class="col-md-6">
                            <div class="link-box history main-blocks__content"
                                 style="background-image:url(<?= get_the_post_thumbnail_url($stories[1]->ID) ?>);">
                                <h3 class="link-box__title history__title">
                                    <?= $stories[1]->post_title ?>
                                </h3>
                                <div class="overlay">
                                    <a href="<?= $stories[1]->guid ?>">
                                        <img class="overlay-img"
                                             src="<?php echo $theme_path_uri; ?>/img/main-links/logo-link.png">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="link-box ideas main-blocks__content"
                                 style="background-image:url(<?= get_the_post_thumbnail_url($stories[2]->ID) ?>);">
                                <h3 class="link-box__title ideas__title">
                                    <?= $stories[2]->post_title ?>
                                </h3>
                                <div class="overlay">
                                    <a href="<?= $stories[2]->guid ?>">
                                        <img class="overlay-img"
                                             src="<?php echo $theme_path_uri; ?>/img/main-links/logo-link.png">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 main-blocks-right__row">
                            <div class="link-box motivation main-blocks__content"
                                 style="background-image:url(<?= get_the_post_thumbnail_url($stories[3]->ID) ?>);">
                                <h3 class="link-box__title motivation__title">
                                    <?= $stories[3]->post_title ?>
                                </h3>
                                <div class="overlay">
                                    <a href="<?= $stories[3]->guid ?>">
                                        <img class="overlay-img"
                                             src="<?php echo $theme_path_uri; ?>/img/main-links/logo-link.png">
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 main-blocks-right__row">
                            <div class="link-box people main-blocks__content"
                                 style="background-image:url(<?= get_the_post_thumbnail_url($stories[4]->ID) ?>);">
                                <h3 class="link-box__title people__title">
                                    <?= $stories[4]->post_title ?>
                                </h3>
                                <div class="overlay">
                                    <a href="<?= $stories[4]->guid ?>">
                                        <img class="overlay-img"
                                             src="<?php echo $theme_path_uri; ?>/img/main-links/logo-link.png">
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="overlay">
        <?php endif; ?>
