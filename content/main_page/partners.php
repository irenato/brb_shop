<?php
/**
 * Created by BitLab
 * Date: 09.11.2016
 */
?>
<?php $partner_block = PTP::getPartners();
if ($partner_block) :
    $partners = get_field('partners_block_items', $partner_block[0]->p2p_from);
    ?>
    <?php if ($partners): ?>
    <!--партнеры-->
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="main-blocks about-us col-md-12">
                    <h2 class="main-blocks__title">партнеры</h2>
                    <div class="partners-slider">
                        <ul class="<?= (count($partners) >= 4) ? 'lightSlider' : 'sliderNewBox' ?> card-box card-box">
                            <?php foreach ($partners as $partner) : ?>
                                <li class="lslide">
                                    <div class="card-box__wrapp">
                                        <?php if ($partner['link']): ?>
                                        <a href="<?= $partner['link'] ?>">
                                            <?php endif ?>
                                            <img src="<?= $partner['logo'] ?>" height="379" width="281">
                                            <?php if ($partner['link']): ?>
                                        </a>
                                    <?php endif ?>
                                        <div class="bottom-menu">
                                            <h4 class="bottom-menu__name"><?= $partner['title'] ?></h4>
                                            <p class="bottom-menu__name-desc"><?= $partner['desription'] ?></p>
                                        </div>
                                    </div>
                                </li>

                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hidden-sm hidden-xs separator-line"></div>
<?php endif; ?>
<?php endif; ?>
