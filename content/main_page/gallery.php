<?php
/**
 * Created by BitLab
 * Date: 09.11.2016
 */
?>
<?php $images = get_field('galery', BRANCH); ?>
<?php if ($images) : ?>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="main-blocks about-us col-md-12">
                <h2 class="main-blocks__title">фотоотчеты</h2>


                <div class="myNewModal">
<!--                    --><?php //$images = get_field('galery', BRANCH); ?>
<!--                    --><?php //if ($images) : ?>
                        <?php $i = 0; ?>
                        <?php foreach ($images as $image) : ?>
                            <?php ++$i; ?>
                            <div class="modal fade" id="myModal<?= $i ?>" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel"><?= $image['title'] ?></h4>
                                        </div>
                                        <div class="modal-body">
                                            <img src="<?= $image['galery_item'] ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
<!--                    --><?php //endif; ?>
                </div>


                <div class="photo-slider">
                    <ul class="lightSlider card-box">
<!--                        --><?php //$images = get_field('galery', BRANCH); ?>
<!--                        --><?php //if ($images) : ?>
                            <?php $i = 0; ?>
                            <?php foreach ($images as $image) : ?>
                                <?php ++$i; ?>
                                <li>
                                    <div class="card-box__wrapp" data-toggle="modal" data-target="#myModal<?= $i ?>">
                                        <img src="<?= $image['galery_item'] ?>">
                                        <div class="bottom-menu">
                                            <h4 class="bottom-menu__name"><?= $image['title'] ?></h4>
                                            <p class="bottom-menu__name-desc"><?= $image['date'] ?></p>
                                            <a class="bottom-menu__link" href="#">Посмотреть фотоотчет</a>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
<!--                        --><?php //endif; ?>

                    </ul>
                </div>
<!--                <div class="wrapp-check-online">-->
<!--                    <a class="check-online ms_booking" data-url="https://yclients.com/booking/34027/2/1" href="#"-->
<!--                       onclick="ga('send', 'event', { eventCategory: 'запись онлайн', eventAction: 'заказ'});">Запись-->
<!--                        онлайн</a>-->
<!--                </div>-->
            </div>
        </div>
    </div>
</div>

<div class="hidden-sm hidden-xs separator-line"></div>

<?php endif; ?>