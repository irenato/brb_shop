<?php
/**
 * User: Максим Руденко
 * Date: 29.02.2016
 */
$options = get_option('services');
?>

<div class="container" id="serviceOfUs">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <?php if (isset($options['service'])):
                $array = $options['service'];
                $array_count = count($options['service']);
                $array_count_1 = (int)($array_count / 2) + $array_count % 2;
                $array_count_2 = $array_count - $array_count_1;
                $services_slugs = filter_services_id_by_filial();
                ?>
                <div class="main-blocks about-us col-md-12">
                    <h2 class="main-blocks__title">Услуги</h2>
                    <div class="services-price">
                        <ul>
                            <?php
                            $z = 0;
                            for ($i = 0; $i < $array_count_1; $i++):
                                $service = $array[$i];
                                $title = isset($service['title']) ? $service['title'] : '';
                                $price = isset($service['price']) ? $service['price'] : '';
                                $url = isset($service['url']) ? $service['url'] : '#';
                                $slug = create_slug_from_services_url($url);

                                ?>
                                <?php if (in_array($slug, $services_slugs)) : ?>
                                <li>
                                    <a href="<?php echo $url; ?>"><?php echo esc_attr($title); ?> <span
                                            class="price-servCLL"><?php echo esc_attr($price); ?></span></a>
                                </li>

                            <?php endif; ?>
                                <?php
                            endfor;
                            ?>

                        </ul>
                    </div>
<!--                    <div class="wrapp-check-online">-->
<!--                        <a class="check-online" href="#">Запись онлайн</a>-->
<!--                    </div>-->
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
