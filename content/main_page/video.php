<?php
/**
 * User: Максим Руденко
 * Date: 01.03.2016
 */
$theme_path_uri = get_template_directory_uri();
?>

<div class="container">
    <div id="videoBlock">
        <div>
            <div>

                <script type="text/javascript">

                    function vidplay() {
                        var video = document.getElementById("video");
                        var button = document.getElementById("play");
                        var buttonBar = document.getElementById("buttonbar");
                        if (video.paused) {
                            video.play();
                            button.style.opacity = '0';
                            button.style.height = '100%';
                            button.style.width = '100%';
                            button.style.position = 'static';
                            buttonBar.style.width = '100%';
                            buttonBar.style.height = '100%';
                            buttonBar.style.position = 'absolute';
                            buttonBar.style.width = '100%';
                            buttonBar.style.height = '100%';
                            buttonBar.style.top = '0';
                            buttonBar.style.right = '0';
                            buttonBar.style.bottom = '0';
                            buttonBar.style.left = '0';
                        } else {
                            video.pause();
                            button.style.opacity = '1';
                            buttonBar.style.position = 'static';
                            button.style.position = 'absolute';
                            button.style.height = '50px';
                            button.style.width = '50px';
                        }
                        ;
                    }
                    ;

                </script>
                <div id="video-wrapp" class="main-blocks-video">
                    <h2 class="main-blocks__title"></h2>
                    <div class="text-center">
                        <video id="video">
                            <source src="<?php echo esc_url($theme_path_uri); ?>/video/video.mp4" type="video/mp4">
                            <source src="<?php echo esc_url($theme_path_uri); ?>/video/video.ogv" type="video/ogg">
                            <iframe width="100%" height="515" src="<?= get_field('video_on_youtube', BRANCH) ?>"
                                    frameborder="0" allowfullscreen></iframe>
                        </video>
                        <div id="buttonbar" class="videobtn">
                            <button id="play" onclick="vidplay()"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
