<?php
/**
 * User: Максим Руденко
 * Date: 29.02.2016
 */
global $curr_lang;
$options = get_option('theme_settings');
?>
<div class="container">
<div id="contacts">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="main-blocks about-us col-md-12 clearfix">
                <h2 class="main-blocks__title">Контакты:</h2>
                <div class="googleMapsContacts">

                    <?php
                    $options = get_option('theme_settings');

                    $lat = get_field('lat', BRANCH);
                    $lang = get_field('lang', BRANCH);
                    $zoom = "12";
                    $maps_lat = isset($options['maps_lat']) ? $options['maps_lat'] : '';
                    $maps_lng = isset($options['maps_lng']) ? $options['maps_lng'] : '';
                    $maps_zoom = isset($options['maps_zoom']) ? $options['maps_zoom'] : '';
                    $maps_marker = isset($options['maps_marker']) ? $options['maps_marker'] : '';

                    if ($maps_zoom != '') {
                        $zoom = $maps_zoom;
                    }

                    $theme_path_uri = get_template_directory_uri();
                    $marker_url = $theme_path_uri . '/img/marker.png';
                    if ($maps_marker) {
                        $marker_url = wp_get_attachment_image_url($maps_marker, 'full');
                    }

                    ?>
                    <div class="map-wrapp" id="map" lat="<?php echo esc_attr($lat); ?>"
                         lang="<?php echo esc_attr($lang); ?>" zoom="<?php echo esc_attr($zoom); ?>"
                         marker="<?php echo esc_attr($marker_url); ?>" style="width: 100%; height: 300px;"></div>
                    <script src="https://maps.googleapis.com/maps/api/js" async defer></script>
                </div>
                <div class="contactsMainBlock">
                    <div class="contacts-wrapper">
                        <div class="address-shop">
                            <a href="<?= get_home_url() . '/branches' ?>">
                                Выберите филиал
                            </a>
                        </div>
                        <?= get_field('landmarks', BRANCH); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

