<?php
/**
 * User: Максим Руденко
 * Date: 01.03.2016
 */
global $curr_lang;
$options = get_option('theme_settings');
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="main-blocks about-us col-md-12">
                <h2 class="main-blocks__title">Популярные товары</h2>
                <div class="tatoo-slider">
                    <ul class="lightSlider card-box">
                        <?php
                        global $post;
                        global $wp_query;
                        $temp = $wp_query;
                        $wp_query = null;

                        $args = array(
                            'post_type' => 'product',
                            'order' => 'DESC',
                            'product_cat' => 'main_shop_page',
                            'ignore_sticky_posts' => true
                        );

                        $wp_query = new WP_Query($args);

                        if ($wp_query->have_posts()) {
                            while ($wp_query->have_posts()) {
                                $wp_query->the_post(); ?>

                                <li>
                                    <div class="card-box__wrapp">
                                        <?php
                                        /**
                                         * woocommerce_before_shop_loop_item hook.
                                         *
                                         * @hooked woocommerce_template_loop_product_link_open - 10
                                         */
                                        do_action('woocommerce_before_shop_loop_item');

                                        /**
                                         * woocommerce_before_shop_loop_item_title hook.
                                         *
                                         * @hooked woocommerce_show_product_loop_sale_flash - 10
                                         * @hooked woocommerce_template_loop_product_thumbnail - 10
                                         */
                                        do_action('woocommerce_before_shop_loop_item_title');

                                        ?>
                                        <div class="bottom-menu">
                                            <?php
                                            /**
                                             * woocommerce_shop_loop_item_title hook.
                                             *
                                             * @hooked woocommerce_template_loop_product_title - 10
                                             */
                                            do_action('woocommerce_shop_loop_item_title');

                                            /**
                                             * woocommerce_after_shop_loop_item_title hook.
                                             *
                                             * @hooked woocommerce_template_loop_rating - 5
                                             * @hooked woocommerce_template_loop_price - 10
                                             */
                                            do_action('woocommerce_after_shop_loop_item_title');

                                            /**
                                             * woocommerce_after_shop_loop_item hook.
                                             *
                                             * @hooked woocommerce_template_loop_product_link_close - 5
                                             * @hooked woocommerce_template_loop_add_to_cart - 10
                                             */
                                            do_action('woocommerce_after_shop_loop_item');
                                            ?>
                                        </div>
                                    </div>
                                </li>
                                <?

                            }
                        }

                        $wp_query = null;
                        $wp_query = $temp;
                        wp_reset_postdata();
                        ?>
                    </ul>
                </div>
<!--                <div class="wrapp-check-online">-->
<!--                    <a class="check-online ms_booking" data-url="https://yclients.com/booking/34027/2/1" href="#"-->
<!--                       onClick="ga('send', 'event', { eventCategory: 'запись онлайн', eventAction: 'заказ'});">--><?php //echo $options['button_check_online' . $curr_lang] ? $options['button_check_online' . $curr_lang] : ''; ?><!--</a>-->
<!--                </div>-->
            </div>
        </div>
    </div>
</div>
