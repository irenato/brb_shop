<?php $options = get_option('theme_settings'); ?>
<?php global $curr_lang; ?>
<?php $f_asked_questions = get_post(2779); ?>
<?php $partners = get_post(2784); ?>
<?php $work = get_post(2787); ?>

<div class="container">
    <div>
        <!--        --><?php
        //        global $page_id;
        //        if ($page_id == 51)
        //            get_template_part('footer_templates/main_footer');
        //        else
        //            get_template_part('footer_templates/small_footer');
        //        ?>
        <!-- вырезать здесь-->
        <footer class="footer main-blocks">
            <div class="row">
                <div class="col-md-12 col-sm-12 footerWorkTime">
                    <p><?= get_field('schedule', BRANCH) ?></p>
<!--                    <p>Работаем ежедневно: с 10:00 до 22:00</p>-->
                </div>
            </div>
            <div class="addressRow">
                <?php $filials = get_posts(array(
                    'numberposts' => 0,
                    'offset' => 0,
                    'orderby' => 'post_date',
                    'order' => 'DESC',
                    'post_type' => 'brb_filial',
                    'post_status' => 'publish'
                )); ?>

                <?php foreach ($filials as $filial) : ?>
                    <div class="addressCol">
                        <p class="footer-address">г. Киев: <span><?= get_field('address', $filial->ID) ?></span></p>
                        <?php $phones = get_field('phones', $filial->ID); ?>
                        <?php if ($phones) : ?>
                            <?php foreach ($phones as $phone) : ?>
                                <p class="phone-number">
                                    <i class="glyphicon glyphicon-earphone"></i> <?= $phone['phone']; ?>
                                </p>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>

            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="address-shop">
                        <a href="<?= $work->guid ?>" class="buttonFooter"><?= $options['footer_button'] ?></a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="address-shop">
                        <a href="<?= $partners->guid ?>" class="buttonFooter"><?= $options['footer_button2'] ?></a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="address-shop ">
                        <a href="<?= $f_asked_questions->guid ?>" class="buttonFooter"><?= $options['footer_button3'] ?></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="skype-call"><i class="fa fa-skype"></i><a
                                href="skype:[<?= get_field('skype', BRANCH) ?>]"><?= get_field('skype', BRANCH) ?>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="nav__social">
                            <ul class="nav__social-list">
                                <?php
                                $social_vk_visible = isset($options['social_vk_visible']) ? $options['social_vk_visible'] : '';
                                $social_vk_url = isset($options['social_vk_url']) ? $options['social_vk_url'] : '';
                                $social_facebook_visible = isset($options['social_facebook_visible']) ? $options['social_facebook_visible'] : '';
                                $social_facebook_url = isset($options['social_facebook_url']) ? $options['social_facebook_url'] : '';
                                $social_instagram_visible = isset($options['social_instagram_visible']) ? $options['social_instagram_visible'] : '';
                                $social_instagram_url = isset($options['social_instagram_url']) ? $options['social_instagram_url'] : '';
                                $social_twitter_visible = isset($options['social_twitter_visible']) ? $options['social_twitter_visible'] : '';
                                $social_twitter_url = isset($options['social_twitter_url']) ? $options['social_twitter_url'] : '';
                                $social_youtube_url = isset($options['social_youtube_url']) ? $options['social_youtube_url'] : '';
                                $social_youtube_visible = isset($options['social_youtube_visible']) ? $options['social_youtube_visible'] : '';
                                ?>
                                <?php if ($social_vk_visible && $social_vk_visible == '1' &&
                                    $social_vk_url != ''
                                ): ?>
                                    <li><a class="fa fa-vk" href="<?php echo esc_url($social_vk_url); ?>"></a></li>
                                <?php endif; ?>
                                <?php if ($social_facebook_visible && $social_facebook_visible == '1' &&
                                    $social_facebook_url != ''
                                ): ?>
                                    <li><a class="fa fa-facebook" href="<?php echo esc_url($social_facebook_url); ?>"></a></li>
                                <?php endif; ?>
                                <?php if ($social_instagram_visible && $social_instagram_visible == '1' &&
                                    $social_instagram_url != ''
                                ): ?>
                                    <li><a class="fa fa-instagram" href="<?php echo esc_url($social_instagram_url); ?>"></a>
                                    </li>
                                <?php endif; ?>
                                <?php if ($social_twitter_visible && $social_twitter_visible == '1' &&
                                    $social_twitter_url != ''
                                ): ?>
                                    <li><a class="fa fa-twitter" href="<?php echo esc_url($social_twitter_url); ?>"></a></li>
                                <?php endif; ?>
                                <?php if ($social_youtube_visible && $social_youtube_visible == '1' &&
                                    $social_youtube_url != ''
                                ): ?>
                                    <li><a class="fa fa-youtube" href="<?php echo esc_url($social_youtube_url); ?>"></a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="email-address"><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:<?= get_field('email') ?>"><?= get_field('email', BRANCH) ?></a></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="all-rights">2016 Киев <a href="/">Mr.Colt©</a> all right reserved</p>
                    </div>
                </div>
        </footer>
        <!-- вырезать здесь-->
    </div>

</div>
</section>

<?php
$theme_path_uri = get_template_directory_uri()
?>
<!-- modal -->
<div class="modal fade" id="questionModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form id="question_form" class="request request-franchise ajaxform" action="" method="post">
                    <h3>Задать вопрос</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" placeholder="Имя*" name="name" required="">
                            <input type="text" placeholder="Телефон*" name="phone" required="">
                            <input type="text" placeholder="E-mail*" name="email" required="">
                        </div>
                        <div class="col-md-6">
                            <textarea placeholder="Вопрос" name="sub"></textarea>
                        </div>
                    </div>
                    <div class="wrapp-check-online">
                        <input id="question_form_sbm_btn" class="check-online btn-submit" type="button"
                               value="Отправить">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal Gift -->
<?php if (is_page(2718)): ?>
    <div class="modal fade" id="giftModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form class="request request-franchise giftAjaxForm" action="" method="post" id="giftModalForm">
                        <h3>Подарочный сертификат на услугу.</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" placeholder="Имя*" name="name" required>
                                <input type="text" placeholder="Телефон*" name="phone" required>
                                <input type="text" placeholder="E-mail*" name="email" required>
                                <input type="hidden" name="gift" value="100 грн">
                                <input type="hidden" name="admin" value="<?= get_option('admin_email') ?>">
                            </div>
                            <div class="col-md-6">
                                <textarea placeholder="Комментарий" name="comment"></textarea>
                            </div>
                        </div>
                        <div class="wrapp-check-online">
                            <input class="check-online btn-submit" type="submit"
                                   value="Заказать">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Gift -->
<?php endif; ?>
<div class="modal fade" id="buyOneClick" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form id="by_one_click" class="request request-franchise ajaxform" action="" method="post">
                    <h3>Купить в один клик</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" placeholder="Имя*" name="name" required="">
                            <input type="text" placeholder="Телефон*" name="phone" required="">
                            <input type="text" placeholder="E-mail*" name="email" required="">
                        </div>
                        <div class="col-md-6">
                            <textarea placeholder="Вопрос" name="sub"></textarea>
                        </div>
                    </div>
                    <div class="wrapp-check-online">
                        <input id="by_one_click_sbm_btn" class="check-online btn-submit" type="button"
                               value="Отправить">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<input type="button" id="bb" data-toggle="modal" data-target="#addCart" style="display:none">
<div class="modal fade" id="addCart" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h2>Спасибо за заказ</h2>
                <p>Товар добавлен в корзину</p>
                <div class="wrapp-check-online">
                    <a href="<?php echo get_site_url() . '/cart'; ?>" type="submit" class="check-online">Корзина</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="buyOneClick" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form class="request request-franchise ajaxform" action="" method="post">
                    <h3>Купить в один клик</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" placeholder="Имя*" name="name" required="">
                            <input type="text" placeholder="Телефон*" name="phone" required="">
                            <input type="text" placeholder="E-mail*" name="email" required="">
                        </div>
                        <div class="col-md-6">
                            <textarea placeholder="Вопрос" name="sub"></textarea>
                        </div>
                    </div>
                    <div class="wrapp-check-online">
                        <input class="check-online btn-submit" type="submit" value="Отправить">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- script -->

<?php wp_footer(); ?>

<script type="text/javascript" src="https://w22025.yclients.com/widgetJS" charset="UTF-8"></script>

<script src="<?php echo esc_url($theme_path_uri); ?>/js/common.js"></script>
<script src="<?php echo esc_url($theme_path_uri); ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8">
    var host = 'https://yclients.com';
    document.write(unescape("%3Cscript src='" + host + "/js/ms.bookingwlink.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript" charset="utf-8">
    MSWidget.initWidget();
</script>
<?php if (is_front_page()): ?>
    <script src="<?php echo esc_url($theme_path_uri); ?>/js/maps.js"></script>
<?php endif; ?>

<!-- script end -->
<!--<script type="text/javascript" charset="utf-8" src="//istat24.com.ua/js/replace.js"></script>-->
<!--<script type="text/javascript">doReplaceIstat(948);</script>-->
</body>
</html>
