<?php
/**
 * User: Максим Руденко
 * Date: 10.03.2016
 * Сайдбар магазина
 */
global $curr_lang;
$options = get_option( 'theme_settings' );
?>
<div class="cart-label">
    <a href="<?php echo esc_url(get_home_url().'/cart'); ?>">
    <div class="cart-label__wrapp cart_totals">
        <?php
        $cart_product_count = WC()->cart->get_cart_contents_count();
        $cart_product_total_price = wp_kses(WC()->cart->get_cart_total(),'');
        ?>
            <p class="quantity"><?php echo $options['cart_widget_product_text'.$curr_lang]?$options['cart_widget_product_text'.$curr_lang]:''; ?> - <span><?php echo esc_html($cart_product_count);?></span></p>
            <p class="total"><?php echo $options['cart_widget_total_text'.$curr_lang]?$options['cart_widget_total_text'.$curr_lang]:''; ?> - <span><?php echo $cart_product_total_price; ?></span></p>
        
    </div>
    </a>
</div>
<nav class="categories">

<h3 class="categories-title"><?php echo $options['shop_category_title'.$curr_lang]?$options['shop_category_title'.$curr_lang]:''; ?></h3>
<?php
    $links = array();
    if(function_exists('woocommerce_get_cat_url')):
    $links =  woocommerce_get_cat_url();
        if(count($links)):
?>
<ul class="categories-nav">
<?php foreach($links as $link): ?>
    <li><a href="<?php echo esc_url($link['url']); ?>"><?php echo esc_html($link['name']); ?></a></li>
<?php endforeach; ?>
</ul>
<?php
        endif;
    endif;
?>
    <?php  get_template_part('content/form/filter-form'); ?>
</nav>

<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar() ) : ?>
<?php endif; ?>
<?php if ( is_active_sidebar( 'shop_sidebar' ) ) :

    dynamic_sidebar( 'shop_sidebar' );

endif; ?>