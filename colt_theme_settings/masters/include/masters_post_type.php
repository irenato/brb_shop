<?php
/**
 * User: Максим Руденкр
 * Date: 29.02.2016
 * Файл отвечает за подключения пользовательских типов - берберов и татуировщиков
 */

function barber_master_post_type()
{
    $labels = array(
        'name' => 'Барбер Мастера',
        'singular_name' => 'Добавить барбер мастера', // админ панель Добавить->Функцию
        'add_new' => 'Добавить барбер мастера',
        'add_new_item' => 'Добавить барбер мастера', // заголовок тега <title>
        'edit_item' => 'Редактировать барбер мастера',
        'new_item' => 'Новый барбер мастер',
        'all_items' => 'Все барбер мастера',
        'view_item' => 'Посмотреть барбер мастера',
        'search_items' => 'Найти барбер мастера',
        'not_found' => 'Барбер мастера не найдено',
        'not_found_in_trash' => 'Барбер мастера не найден в корзине.',
        'menu_name' => 'Барбер Мастера' // ссылка в меню в админке
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true, // показывать интерфейс в админке
        'has_archive' => true,
        'taxonomies' => array(),
        'menu_icon' => 'dashicons-businessman', // иконка в меню
        'menu_position' => 29, // порядок в меню
        'supports' => array('title', 'comments')
    );
    register_post_type('barber-master', $args);
}

add_action('init', 'barber_master_post_type');
function tattoo_master_post_type()
{
    $labels = array(
        'name' => 'Тату Мастера',
        'singular_name' => 'Добавить тату мастера', // админ панель Добавить->Функцию
        'add_new' => 'Добавить тату мастера',
        'add_new_item' => 'Добавить тату мастера', // заголовок тега <title>
        'edit_item' => 'Редактировать тату мастера',
        'new_item' => 'Новый тату мастер',
        'all_items' => 'Все тату мастера',
        'view_item' => 'Посмотреть тату мастера',
        'search_items' => 'Найти тату мастера',
        'not_found' => 'Тату мастер не найден',
        'not_found_in_trash' => 'Тату мастер не найден в корзине.',
        'menu_name' => 'Тату Мастера' // ссылка в меню в админке
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true, // показывать интерфейс в админке
        'has_archive' => true,
        'taxonomies' => array(),
        'menu_icon' => 'dashicons-businessman', // иконка в меню
        'menu_position' => 30, // порядок в меню
        'supports' => array('title', 'comments')
    );
    register_post_type('tattoo-master', $args);
}

add_action('init', 'tattoo_master_post_type');

function brb_filials_post_type()
{
    $labels = array(
        'name' => 'Наши филиалы',
        'singular_name' => 'Добавить филиал', // админ панель Добавить->Функцию
        'add_new' => 'Добавить филиал',
        'add_new_item' => 'Добавить филиал', // заголовок тега <title>
        'edit_item' => 'Редактировать филиал',
        'new_item' => 'Новый филиал',
        'all_items' => 'Все филиалы',
        'view_item' => 'Посмотреть филиал',
        'search_items' => 'Найти филиал',
        'not_found' => 'Филиал не найден',
        'menu_name' => 'Наши филиалы' // ссылка в меню в админке
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true, // показывать интерфейс в админке
        'has_archive' => true,
        'taxonomies' => array(),
        'menu_icon' => 'dashicons-store', // иконка в меню
        'menu_position' => 31, // порядок в меню
        'supports' => array('title', 'comments', 'editor')
    );
    register_post_type('brb_filial', $args);
}

add_action('init', 'brb_filials_post_type');

function brb_promo_post_type()
{
    $labels = array(
        'name' => 'Акции',
        'singular_name' => 'Добавить акцию', // админ панель Добавить->Функцию
        'add_new' => 'Добавить акцию',
        'add_new_item' => 'Добавить акцию', // заголовок тега <title>
        'edit_item' => 'Редактировать акцию',
        'new_item' => 'Новая акция',
        'all_items' => 'Все акции',
        'view_item' => 'Посмотреть акцию',
        'search_items' => 'Найти акцию',
        'not_found' => 'Акция не найдена',
        'menu_name' => 'Акции' // ссылка в меню в админке
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true, // показывать интерфейс в админке
        'has_archive' => true,
        'taxonomies' => array(),
        'menu_icon' => 'dashicons-carrot', // иконка в меню
        'menu_position' => 32, // порядок в меню
        'supports' => array('title', 'comments', 'editor')
    );
    register_post_type('promo', $args);
}

add_action('init', 'brb_promo_post_type');

function brb_partner_post_type()
{
    $labels = array(
        'name' => 'Блок партнеров',
        'singular_name' => 'Добавить блок', // админ панель Добавить->Функцию
        'add_new' => 'Добавить блок',
        'add_new_item' => 'Добавить блок', // заголовок тега <title>
        'edit_item' => 'Редактировать блок',
        'new_item' => 'Новый блок',
        'all_items' => 'Все блоки партнеров',
        'view_item' => 'Посмотреть содержимое блока партнеров',
        'search_items' => 'Найти блок партнеров',
        'not_found' => 'Блок партнеров не найден',
        'menu_name' => 'Блок партнеров' // ссылка в меню в админке
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true, // показывать интерфейс в админке
        'has_archive' => true,
        'taxonomies' => array(),
        'menu_icon' => 'dashicons-groups', // иконка в меню
        'menu_position' => 33, // порядок в меню
        'supports' => array('title')
    );
    register_post_type('brb_partners', $args);
}

add_action('init', 'brb_partner_post_type');

//------------------------------------------
// связывание филиал - барбер-мастер
//------------------------------------------
if (function_exists("_p2p_load")) {
    function vp_post_to_post()
    {
        p2p_register_connection_type(array(
            'name' => 'barber-masters_to_filial',
            'from' => 'barber-master',
            'to' => 'brb_filial'
        ));
    }

    add_action('p2p_init', 'vp_post_to_post');
}

//------------------------------------------
// связывание филиал - тату-мастер
//------------------------------------------
if (function_exists("_p2p_load")) {
    function vp_post_to_post2()
    {
        p2p_register_connection_type(array(
            'name' => 'tattoo-masters_to_filial',
            'from' => 'tattoo-master',
            'to' => 'brb_filial'
        ));
    }

    add_action('p2p_init', 'vp_post_to_post2');
}

//------------------------------------------
// связывание филиал - акции
//------------------------------------------
if (function_exists("_p2p_load")) {
    function vp_post_to_post3()
    {
        p2p_register_connection_type(array(
            'name' => 'promo_to_filial',
            'from' => 'promo',
            'to' => 'brb_filial'
        ));
    }

    add_action('p2p_init', 'vp_post_to_post3');
}

//------------------------------------------
// связывание филиал - тату-мастер
//------------------------------------------
if (function_exists("_p2p_load")) {
    function vp_post_to_post4()
    {
        p2p_register_connection_type(array(
            'name' => 'sliders_to_filial',
            'from' => 'slider',
            'to' => 'brb_filial',
        ));
    }

    add_action('p2p_init', 'vp_post_to_post4');
}

//------------------------------------------
// связывание филиал - услуга(страницы)
//------------------------------------------
if (function_exists("_p2p_load")) {
    function vp_post_to_post5()
    {
        p2p_register_connection_type(array(
            'name' => 'services_to_filial',
            'from' => 'page',
            'from_query_vars' => array(
                'tax_query' => array(
                    array(
                        'taxonomy' => 'category',
                        'field' => 'slug',
                        'terms' => 'brb_services'
                    )
                )
            ),
            'to' => 'brb_filial',
            'title' => 'Услуги'
        ));
    }

    add_action('p2p_init', 'vp_post_to_post5');
}

//------------------------------------------
// связывание филиал - блок партнеров
//------------------------------------------
if (function_exists("_p2p_load")) {
    function vp_post_to_post6()
    {
        p2p_register_connection_type(array(
            'name' => 'partners_to_filial',
            'from' => 'brb_partners',
            'to' => 'brb_filial',
            'cardinality' => 'one-to-one',
        ));
    }

    add_action('p2p_init', 'vp_post_to_post6');
}

//------------------------------------------
// связывание филиал - о_нас(страницы)
//------------------------------------------
if (function_exists("_p2p_load")) {
    function vp_post_to_post7()
    {
        p2p_register_connection_type(array(
            'name' => 'about_us_to_filial',
            'from' => 'page',
            'from_query_vars' => array(
                'tax_query' => array(
                    array(
                        'taxonomy' => 'category',
                        'field' => 'slug',
                        'terms' => 'about_us'
                    )
                )
            ),
            'to' => 'brb_filial',
            'title' => 'О нас (рекомендуемое количество - 5)'
        ));
    }

    add_action('p2p_init', 'vp_post_to_post7');
}

add_action('init', 'category_for_pages');

function category_for_pages()
{
    register_taxonomy_for_object_type('category', 'page');
}

/**
 * @return array
 */
function filter_services_id_by_filial()
{
    $services = get_posts(array(
        'connected_type' => 'services_to_filial',
        'connected_items' => BRANCH,
        'nopaging' => true,
    ));
    $services_id = array();
    foreach ($services as $service) {
        array_push($services_id, $service->post_name);
    }
    return $services_id;
}

/**
 * @param $url
 * @return mixed
 */
function create_slug_from_services_url($url)
{
    $slug = (explode('/', $url));
    return $slug[count($slug) - 2];
}

