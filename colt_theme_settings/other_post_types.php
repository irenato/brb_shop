<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 10.12.16
 * Time: 17:15
 */

function brb_summary_p_type()
{
    $labels = array(
        'name' => 'Резюме',
        'singular_name' => 'Добавить резюме',
        'add_new' => 'Добавить резюме',
        'add_new_item' => 'Добавить резюме',
        'edit_item' => 'Редактировать резюме',
        'new_item' => 'Новое резюме',
        'all_items' => 'Все резюме',
        'view_item' => 'Посмотреть резюме',
        'search_items' => 'Найти резюме',
        'not_found' => 'Резюме не найдено',
        'menu_name' => 'Резюме'
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'has_archive' => true,
        'taxonomies' => array(),
        'menu_icon' => 'dashicons-nametag',
        'menu_position' => 34,
        'supports' => array('title', 'editor')
    );
    register_post_type('brb_summary', $args);
}

add_action('init', 'brb_summary_p_type');