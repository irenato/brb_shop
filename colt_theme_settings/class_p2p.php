<?php
/**
 * Created by BitLab
 * Date: 09.11.2016
 */

class PTP
{
    public static function getBarbers($id = BRANCH){
        global $wpdb;
        $table_name = $wpdb->prefix . 'p2p';
        $sql = "SELECT `p2p_from` FROM $table_name WHERE `p2p_to` = $id AND `p2p_type` = 'barber-masters_to_filial'";
        return $wpdb->get_results($sql);
    }

    public static function getTattoMasters($id = BRANCH){
        global $wpdb;
        $table_name = $wpdb->prefix . 'p2p';
        $sql = "SELECT `p2p_from` FROM $table_name WHERE `p2p_to` = $id AND `p2p_type` = 'tattoo-masters_to_filial'";
        return $wpdb->get_results($sql);
    }

    public static function getActions($id = BRANCH){
        global $wpdb;
        $table_name = $wpdb->prefix . 'p2p';
        $sql = "SELECT `p2p_from` FROM $table_name WHERE `p2p_to` = $id AND `p2p_type` = 'promo_to_filial'";
        return $wpdb->get_results($sql);
    }

    public static function getServices($id = BRANCH){
        global $wpdb;
        $table_name = $wpdb->prefix . 'p2p';
        $sql = "SELECT `p2p_from` FROM $table_name WHERE `p2p_to` = $id AND `p2p_type` = 'services_to_filial'";
        return $wpdb->get_results($sql);
    }

    public static function getPartners($id = BRANCH){
        global $wpdb;
        $table_name = $wpdb->prefix . 'p2p';
        $sql = "SELECT `p2p_from` FROM $table_name WHERE `p2p_to` = $id AND `p2p_type` = 'partners_to_filial'";
        return $wpdb->get_results($sql);
    }

}