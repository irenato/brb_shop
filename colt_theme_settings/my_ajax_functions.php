<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 10.12.16
 * Time: 21:14
 */

add_action('wp_ajax_nopriv_addSummary', 'addSummary');
add_action('wp_ajax_addSummary', 'addSummary');

function addSummary()
{
    global $wpdb;
    $name = stripcslashes(trim($_POST['username']));
    $email = stripcslashes(trim($_POST['email']));
    $link= stripcslashes(trim($_POST['link']));
    $phone = stripcslashes(trim($_POST['phone']));
    $city = stripcslashes(trim($_POST['city']));
    $summary = stripcslashes(trim($_POST['summary']));
    $fields = array(
        'post_type' => 'brb_summary',
        'post_title' => $name,
        'post_content' => $summary,
        'post_status' => 'pending'
    );
    $post_id = wp_insert_post($fields);
    if (update_post_meta($post_id, 'city', $city) && update_post_meta($post_id, 'email', $email) && update_post_meta($post_id, 'phone', $phone) && update_post_meta($post_id, 'link', $link)) {
        print('done!');
        die();
    }
}