<?php
/**
 * Created by PhpStorm.
 * User: Khalimov Renato
 * Date: 13.11.2016
 * Time: 12:20
 */

function remove_menus_bloggood_ru(){
    remove_menu_page( 'index.php' );
    remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category' );
    remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag' );
    remove_menu_page('wpcf7', 'edit.php?post_type=wpcf7' );
//    remove_menu_page( 'edit.php' );
    remove_menu_page( 'upload.php' );
//    remove_menu_page( 'edit.php?post_type=page' );
//    remove_menu_page( 'edit-comments.php' );
    remove_menu_page( 'themes.php' );
    remove_menu_page( 'plugins.php' );

    remove_menu_page( 'users.php' );
//    remove_menu_page( 'tools.php' );
    remove_menu_page( 'options-general.php' );
}
add_action( 'admin_menu', 'remove_menus_bloggood_ru' );

add_action( 'admin_init', 'wpse_136058_remove_menu_pages' );
function wpse_136058_remove_menu_pages() {

    remove_menu_page( 'edit.php?post_type=acf' );
    remove_menu_page( 'wpcf7' );
}