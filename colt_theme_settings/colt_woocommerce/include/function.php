<?php
/**
 * Добавляем свой вывод миниатбры в каталог товаров woocommerce
 */
add_action('woocommerce_before_shop_loop_item_title','colt_woocommerce_template_loop_product_thumbnail');
function colt_woocommerce_template_loop_product_thumbnail(){
global $post;
$template = "<img width='280px' src='%src%' alt='%alt%' />";

$img_src = get_the_post_thumbnail_url($post->ID,array(280,425));
$img_alt = get_the_title($post->ID);

$template = str_replace('%src%',$img_src,$template);
$template = str_replace('%alt%',$img_alt,$template);
return print $template;
}

/**
 * Добавляем свой вывод title ы каталог товаров woocommerce
 */
add_action('woocommerce_shop_loop_item_title','colt_woocommerce_template_loop_product_title');
function colt_woocommerce_template_loop_product_title(){
    global $post;
    $template = '<h4 class="bottom-menu__name">'.get_the_title($post->ID).'</h4>';

    return print $template;
}

/**
 * Оптимизация скриптов WooCommerce
 * Убираем WooCommerce Generator tag, стили, и скрипты для страниц, не относящихся к WooCommerce.
 */
add_action( 'wp_enqueue_scripts', 'child_manage_woocommerce_styles', 99 );

function child_manage_woocommerce_styles() {
    //убираем generator meta tag
    remove_action( 'wp_head', array( isset($GLOBALS['woocommerce'])?$GLOBALS['woocommerce']:'', 'generator' ) );

    //для начала проверяем, активен ли WooCommerce, дабы избежать ошибок
    if ( function_exists( 'is_woocommerce' ) ) {
        //отменяем загрузку скриптов и стилей
        if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
            wp_dequeue_style( 'woocommerce_frontend_styles' );
            wp_dequeue_style( 'woocommerce_fancybox_styles' );
            wp_dequeue_style( 'woocommerce_chosen_styles' );
            wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
            wp_dequeue_script( 'wc_price_slider' );
            wp_dequeue_script( 'wc-single-product' );
            wp_dequeue_script( 'wc-add-to-cart' );
            wp_dequeue_script( 'wc-cart-fragments' );
            wp_dequeue_script( 'wc-checkout' );
            wp_dequeue_script( 'wc-add-to-cart-variation' );
            wp_dequeue_script( 'wc-single-product' );
            wp_dequeue_script( 'wc-cart' );
            wp_dequeue_script( 'wc-chosen' );
            wp_dequeue_script( 'woocommerce' );
            wp_dequeue_script( 'prettyPhoto' );
            wp_dequeue_script( 'prettyPhoto-init' );
            wp_dequeue_script( 'jquery-blockui' );
            wp_dequeue_script( 'jquery-placeholder' );
            wp_dequeue_script( 'jqueryui' );
        }
    }

}
add_filter( 'add_to_cart_text', 'woo_custom_product_add_to_cart_text' );            // < 2.1
add_filter( 'woocommerce_product_add_to_cart_text', 'woo_custom_product_add_to_cart_text' );  // 2.1 +
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_product_add_to_cart_text' );

function woo_custom_product_add_to_cart_text() {

    return __( 'Купити', 'woocommerce' );

}
function woocommerce_get_cat_url(){
    $args = array(
        'type'                     => 'product',
        'hide_empty'               => 0,
        'hierarchical'             => 0,
        'taxonomy'                 => 'product_cat',
        'pad_counts'               => false,
        'exclude'                  => '66'
    );
    $links = array();
    $categories = get_categories( $args );
    if( $categories ){
        foreach( $categories as $cat ):
            array_push($links, array('url'=>get_term_link( $cat->slug, 'product_cat' ),'name'=>$cat->name));
        endforeach;
    }
    return $links;
}

/**
 *  Добавление таксономии для хранения производителя
 */
function add_brand_taxonomies() {
    $labels = array(
        'name'              => 'Бренд',
        'singular_name'     => 'Бренд',
        'search_items'      => 'Поиск бренда',
        'all_items'         => 'Все бренды',
        'parent_item'       => 'Parent Genre',
        'parent_item_colon' => 'Parent Genre:',
        'edit_item'         => 'Изменить бренд',
        'update_item'       => 'Обновить бренд',
        'add_new_item'      => 'Добавить новый бренд',
        'new_item_name'     => 'Название нового бренд',
        'menu_name'         => 'Бренд',
    );
    register_taxonomy('product_brand',
        array('product'),
        array(
            'labels' => $labels,
            'hierarchical' => true,
            'public' => true,
            'show_in_nav_menus' => true,
            'show_ui' => 1,
            'show_tagcloud' => false,
            'update_count_callback' => '_update_post_term_count',
            'query_var' => 'product_brand',
            'rewrite' => array(
                'slug' => 'product_brand', // ярлык
                'hierarchical' => false
            ),
        )
    );
}
add_action( 'init', 'add_brand_taxonomies', 0 );

/**
 *  Функция вывода списка брендов
 */
function brand_list_view($name, $value){

    $value = trim($value);
    $args = array(
        'type'         => 'product',
        'orderby'      => 'name',
        'order'        => 'ASC',
        'hide_empty'   => 0,
        'hierarchical' => 0,
        'number'       => 0,
        'taxonomy'     => 'product_brand',
        'pad_counts'   => false
    );
    $select  = "";
    if(strcmp($value,  '') == 0){
        $select = 'select = "selected"';
    }
    $option = '<option value="" '.$select.'>По бренду</option>';
    $categories = get_categories( $args );
    if( $categories ){
        foreach( $categories as $cat ){
            $select = '';
            if(strcmp($value,  $cat->name) == 0){
                $select = 'selected = "selected"';
            }
            $option .= '<option '.$select.' value="' .$cat->name. '">';
            $option .= $cat->name;
            $option .= '</option>';
        }
    }
    return '
    <select class="filter-open__list filter-open__price" name="'.$name.'">
      '.$option.'
    </select>
  ';
}

/**
 *  Добавление дополнительных полей к товару
 */
function woocommerce_additional_fields(){
    global $post;
    wp_nonce_field( basename( __FILE__ ), 'woo-additional-field' );

    $door_brand = get_post_meta($post->ID,'_door_brand', true)?get_post_meta($post->ID,'_door_brand', true):'';
    ?>

    <table class="add-box">
        <tr valign="top">
            <td><h3>Бренд товара:</h3></td>
            <td><?php echo brand_list_view('_door_brand', $door_brand); ?></td>
        </tr>
    </table>

    <?php
}

function woocommerce_additional_fields_save ( $post_id ) {
    // проверяем, пришёл ли запрос со страницы с метабоксом
    if ( !isset( $_POST['woo-additional-field'] )
        || !wp_verify_nonce( $_POST['woo-additional-field'], basename( __FILE__ ) ) )
        return $post_id;
    // проверяем, является ли запрос автосохранением
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return $post_id;
    // проверяем, права пользователя, может ли он редактировать записи
    if ( !current_user_can( 'edit_post', $post_id ) )
        return $post_id;
    // теперь также проверим тип записи
    $post = get_post($post_id);

    if ($post->post_type == 'product')
    {
        update_post_meta($post->ID,'_door_brand', $_POST['_door_brand']);
    }
    return $post_id;
}
add_action('save_post', 'woocommerce_additional_fields_save');
function load_woocommerce_additional_fields() {
    add_meta_box('', 'Дополнительные данные о товаре', 'woocommerce_additional_fields', 'product', 'normal', 'high');
}
add_action( 'admin_menu', 'load_woocommerce_additional_fields' );
add_action( 'woocommerce_archive_description', 'loop_fun');
function loop_fun(){
    global $wp_query;
    $args = array();

    $args = array_merge($args, $wp_query->query);
    $args = array_merge($args, $wp_query->query_vars);
    $args['meta_query'] = array(
        'relation' => 'AND'
    );
    $args['order']    = 'ASC';
    if(isset($_GET['order']) && (!strcmp($_GET['order'],'ASC') || !strcmp($_GET['order'],'DESC'))){
        $args['order'] = $_GET['order'];
    }
    $args['meta_key'] = '_regular_price';
    $args['orderby']  = 'meta_value_num';


    if(isset($_GET['filter_price_to']) && $_GET['filter_price_to']!=''
        && isset($_GET['filter_price_from']) && $_GET['filter_price_from']!=''){
        if($_GET['filter_price_to'] < $_GET['filter_price_from']){
            $temp                      = $_GET['filter_price_to'];
            $_GET['filter_price_to']   = $_GET['filter_price_from'];
            $_GET['filter_price_from'] = $temp;
        }
    }

    if(isset($_GET['filter_price_to']) && $_GET['filter_price_to'] != 0) {
        if (isset($_GET['filter_price_to']) && $_GET['filter_price_to'] != '') {
            $args['meta_query'][] = array(
                'key' => '_regular_price',
                'value' => $_GET['filter_price_to'],
                'type' => 'numeric',
                'compare' => '<='
            );
        }
    }
    if (isset($_GET['filter_price_from']) && $_GET['filter_price_from'] != '') {
        $args['meta_query'][] = array(
            'key' => '_regular_price',
            'value' => $_GET['filter_price_from'],
            'type' => 'numeric',
            'compare' => '>'
        );
    }

    if(isset($_GET['brand']) && $_GET['brand']!=''){
        $args['meta_query'][] = array(
            'key'     => '_door_brand',
            'value'   => $_GET['brand']
        );
    }

    $wp_query = new WP_Query($args);
}