<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 10.12.16
 * Time: 20:08
 */

function brb_my_scripts()
{
    if (is_page(2787)) {
        wp_enqueue_script('brb_my_mask', get_template_directory_uri() . '/js/jquery.maskedinput.js', false, '', false);
        wp_enqueue_script('brb_my_scripts', get_template_directory_uri() . '/js/action.js', false, '', false);
        wp_localize_script('brb_my_scripts', 'brb_my_scripts_ajax', array(
            'ajax_url' => admin_url('admin-ajax.php')
        ));
    }
}

add_action('wp_enqueue_scripts', 'brb_my_scripts');
