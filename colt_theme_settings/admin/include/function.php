<?php
/**
 * Файл отвечает за Функции для Опций темы
 * User: Руденко Максим
 * Date: 25.02.2016
 */


//Регистрация функции настроек
function theme_settings_init(){
    register_setting( 'theme_settings', 'theme_settings' );
}
// Добавление настроек в меню страницы
function add_settings_page() {
    add_menu_page( __( 'Опции темы' ), __( 'Опции темы' ), 'manage_options', 'settings', 'theme_settings_page');
}
//Добавление действий
add_action( 'admin_init', 'theme_settings_init' );
add_action( 'admin_menu', 'add_settings_page' );


/**
 * Функция добавляет поле для выбора и загрузки фотографии, картинки
 * Аргументы:
 * Имя функции, Значение, Ширина (не обязательный), Высота (не обязательный)
 * Возвращаемое значение: поле для выбора картинки загружаемой из или в медиабиблиотеку
 * !!Требует реставрации
 * Дата: 25.02.2016
 */
function tf_img_upload($name, $value = '', $w = '115px', $h = 'auto') {
    $default = colt_theme_settings_directory_uri . '/admin/images/no-logo.png';
    if( $value ) {
        $image_attributes = wp_get_attachment_image_src( $value, 'full' );
        $src = $image_attributes[0];
    } else {
        $src = $default;
    }


    return '
	<div>
		<img data-src="'.$default.'" src="'.$src.'" width="'.$w.'" height="'.$h.'" />
		<div>
			<input type="hidden" name="'.$name.'" id="'.$name.'" value="'.$value.'" />
			<button type="button" class="upload_image_button button">Загрузить</button>
			<button type="button" class="remove_image_button button">&times;</button>
		</div>
	</div>
	';
}

/**
 * Функция проверяет существование переменной
 * Аргументы: Переменная
 * Возвращаемое значение: Значение переменной или в случае отсутствия пустая строка
 * Дата: 26.02.2016
 */
function tf_variable_exist($variable){
    if(isset($variable)){
        return $variable;
    }
    else{return '';}
}

/**
 * @param $name атрибут name елемента
 * @param $value выбраное значение
 * @param string $parent_name имя родительской страницы
 * @return string список страниц
 *
 * Функция отвечает за вывод выпадающего списка страниц
 */
function tf_view_pages($name, $value, $parent_name = ''){
    $value = isset($value)?$value:'';
    $args = "";
    $page_id = -1;
    $pages = get_pages();
    if($parent_name != ''){
        foreach ( $pages as $page ) {
            if(strcmp($page->post_name, $parent_name) == 0){
                $page_id = $page->ID;
                $args = array(
                    'child_of' => $page_id,
                    'hierarchical' => 0
                );
                break;
            }
        }
    }
    $select = '';
    if(strcmp($value,  '') == 0){
        $select = 'select = "selected"';
    }
    $option = '<option value="" '.$select.'>Не выбрано</option>';

    $pages = get_pages($args);
    foreach ( $pages as $page ) {
        $select = '';
        if(strcmp($value,  get_permalink($page->ID)) == 0){
            $select = 'selected = "selected"';
        }
        $option .= '<option '.$select.' value="' .get_permalink($page->ID). '">';
        $option .= $page->post_title.' / '.$page->post_name;
        $option .= '</option>';
    }

    return '
    <select name="'.$name.'">
      '.$option.'
    </select>
  ';
}
function tf_view_pages_with_id($name, $value, $parent_name = ''){
    $value = isset($value)?$value:'';
    $args = "";
    $page_id = -1;
    $pages = get_pages();
    if($parent_name != ''){
        foreach ( $pages as $page ) {
            if(strcmp($page->post_name, $parent_name) == 0){
                $page_id = $page->ID;
                $args = array(
                    'child_of' => $page_id,
                    'hierarchical' => 0
                );
                break;
            }
        }
    }
    $select = '';
    if(strcmp($value,  '') == 0){
        $select = 'select = "selected"';
    }
    $option = '<option value="" '.$select.'>Не выбрано</option>';

    $pages = get_pages($args);
    foreach ( $pages as $page ) {
        $select = '';
        if(strcmp($value,  $page->ID) == 0){
            $select = 'selected = "selected"';
        }
        $option .= '<option '.$select.' value="' .$page->ID. '">';
        $option .= $page->post_title.' / '.$page->post_name;
        $option .= '</option>';
    }

    return '
    <select name="'.$name.'">
      '.$option.'
    </select>
  ';
}

function tf_view_woo_cat($name, $value){
        $value = isset($value)?$value:'';
        $args = array(
            'type'         => 'product',
            'orderby'      => 'name',
            'order'        => 'ASC',
            'hide_empty'   => 1,
            'hierarchical' => 'true',
            'exclude'      => '',
            'include'      => '',
            'number'       => 0,
            'taxonomy'     => 'product_cat'
            // полный список параметров смотрите в описании функции http://wp-kama.ru/function/get_terms
        );


        $categories = get_categories( $args );

        if(strcmp($value,  '') == 0){
            $select = 'select = "selected"';
        }
        $option = '<option value="" '.$select.'>Не выбрано</option>';

        if( $categories ){
            foreach( $categories as $cat ){
                $url = get_term_link($cat->slug, 'product_cat');
                $select = '';
                if(strcmp($value,  $url) == 0){
                    $select = 'selected = "selected"';
                }
                $option .= '<option '.$select.' value="' .$url. '">';
                $option .= $cat->name;;
                $option .= '</option>';

            }
            return '<select name="'.$name.'">
                      '.$option.'
                    </select>
                  ';

        }else{
            return 0;
        }

}
/**
 * Функция вывода языковой конструкции
 * id, lang[ru, ua, eng] => name, value, title  multiply
 */
function view_lang_input($args){
    if(is_array($args) && isset($args['id']) && isset($args['lang'])){
        $block = array('ru'=>'','ua'=>'', 'eng'=>'');
        if(isset($args['multiply']) && (bool)$args['multiply'] == true){
            //Несколько полей
        }
        else{
            foreach ($args['lang'] as $key => $value){
               if($key != 'title') {
                   $block[$key] = '';
                   if (isset($args['lang']['title']) && $args['lang']['title'] != '') {
                       $block[$key] = "<h3>{$args['lang']['title']}</h3>";
                   }
                   if (isset($value['element']) && $value['element'] != '') {
                       $block[$key] .= $value['element'];
                       continue;
                   }
                   if(isset($value['element_type'])){
                       if(strcmp($value['element_type'],'textarea') == 0){
                           $value['value'] = isset($value['value'])?$value['value']:'';
                           $value['name']  = isset($value['name'])?$value['name']:'';
                           $block[$key] .= "
                                <textarea  class='textarea-admin' name='{$value['name']}' >{$value['value']}</textarea>
                               ";
                       }
                       if(strcmp($value['element_type'],'input') == 0){
                           $value['value'] = isset($value['value'])?$value['value']:'';
                           $block[$key] .= "
                                <input type='input' class='input-admin' name='{$value['name']}' value='{$value['value']}'>
                               ";
                       }
                   }
                   else {
                       $value['value'] = isset($value['value'])?$value['value']:'';
                       $block[$key] .= "
                        <input type='input' class='input-admin' name='{$value['name']}' value='{$value['value']}'>
                        ";
                   }

               }

            }
        }
        $id = $args['id'];
        $template = "
            <div id=\"$id\" class=\"tabs2\">
                <ul>
                    <li>Рус.</li>
                    <li>Укр.</li>
                    <li>Англ.</li>
                </ul>
                <div>
                    <div>{$block['ru']}</div>
                    <div>{$block['ua']}</div>
                    <div>{$block['eng']}</div>
                </div>
            </div>
            <script>
                $(function(){
                    $(\"#$id\").lightTabs();
                });
            </script>
        ";

        return $template;
    }

    return 0;
}

/**
 * Функция вывода меню в админку
 */
function view_menu_select($name, $value){
    if(isset($name) && $name != ''){
        $value = isset($value)?$value:'';
        $block = '';
        $args = array( 'hide_empty' => false, 'orderby' => 'name' );
        $menus = wp_get_nav_menus( $args );
        if(isset($menus)):
            foreach($menus as $item):
                $selected = '';
                if ( strcmp($item->term_id, $value) == 0){
                    $selected = 'selected="selected"';
                }

                $block .= "<option value='$item->term_id' $selected>$item->name</option>";

            endforeach;
        endif;

        $template = "
            <select name=\"$name\">
                <option value=\"-1\" >Не выбрано</option>
                $block
            </select>
        ";

        return $template;
    }
    return 0;
}

/**
 * Функция вывода категорий в админку
 */

function view_category_select($name, $value){
    if(isset($name) && $name != ''){
        $value = isset($value)?$value:0;

        $args = array(
            'type'         => 'post',
            'child_of'     => 0,
            'parent'       => '',
            'orderby'      => 'name',
            'order'        => 'ASC',
            'hide_empty'   => 1,
            'hierarchical' => 1,
            'exclude'      => '',
            'include'      => '',
            'number'       => 0,
            'taxonomy'     => 'category',
            'pad_counts'   => false
        );

        $block = '';

        $categories = get_categories( $args );
        if( $categories ){
            foreach( $categories as $cat ){
                $selected = '';
                if ( strcmp($cat->slug, $value) == 0){
                    $selected = 'selected="selected"';
                }

                $block .= "<option value='$cat->slug' $selected>$cat->name</option>";
            }
        }

        $template = "
            <select name=\"$name\">
                <option value=\"0\" >Не выбрано</option>
                $block
            </select>
        ";

        return $template;
    }
    return 0;
}