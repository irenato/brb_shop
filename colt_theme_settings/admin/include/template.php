<?php
/**
 * Файл отвечает за отображение шаблона страницы Опции темы
 * User: Руденко Максим
 * Date: 25.02.2016
 */

function theme_settings_page() {
    global $select_options; if ( ! isset( $_REQUEST['settings-updated'] ) ) $_REQUEST['settings-updated'] = false;
    ?>
    <?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
        <div id="message" class="updated">
            <p><strong>Настройки сохранены</strong></p>
        </div>
    <?php endif; ?>
    <div class="theme-option-main-block">
        <br>
        <div>

            <form method="post" action="options.php">
                <p><input name="submit" id="submit" class="button button-primary my-button-primary" value="Сохранить" type="submit"></p>
                <?php settings_fields( 'theme_settings' ); ?>
                <?php $options = get_option( 'theme_settings' ); ?>

                <div id="container">
                    <div class="tabs">
                        <input id="tab1" type="radio" name="tabs" checked>
                        <label for="tab1" title="">Общие настройки</label>

                        <input id="tab2" type="radio" name="tabs">
                        <label for="tab2" title="">Контакты</label>

                        <input id="tab3" type="radio" name="tabs">
                        <label for="tab3" title="">Настройки страниц</label>

                        <input id="tab4" type="radio" name="tabs">
                        <label for="tab4" title="">Настройка расширений</label>

                        <section id="content1">
                            <table class="theme-option-table">
                                <tr valign="top"><td colspan='2'><h1>Настройка шапки сайта</h1></td></tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <tr valign="top">
                                    <td>
                                        <h3>Логотип:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[logo]', tf_variable_exist($options['logo']));
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Фавикон:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[favicon]', tf_variable_exist($options['favicon']), 16, 16);
                                        ?>
                                    </td>
                                </tr>
                            </table>

                            <table class="theme-option-table">
                                <tr valign="top"><td colspan='2'><h1>Настройка подвала сайта</h1></td></tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <td><span>Текст кнопки задать вопрос:</span></td>
                                <td colspan="1">
                                    <?php
                                    $args = array(
                                        'id' => 'ask_question_button',
                                        'lang' => array(
                                            //'title' => 'Предыдущая статья:',
                                            'ru' => array(
                                                'name' => 'theme_settings[ask_question_button]',
                                                'value' => $options['ask_question_button']?$options['ask_question_button']:''
                                            ),
                                            'ua' => array(
                                                'name' => 'theme_settings[ask_question_button_ua]',
                                                'value' => $options['ask_question_button_ua']?$options['ask_question_button_ua']:''
                                            ),
                                            'eng' => array(
                                                'name' => 'theme_settings[ask_question_button_eng]',
                                                'value' => $options['ask_question_button_eng']?$options['ask_question_button_eng']:''
                                            )
                                        )
                                    );
                                    echo view_lang_input($args);
                                    ?>
                                </td>
                            </table>

                            <table class="theme-option-table">
                                <tr valign="top"><td colspan='2'><h1>Настройка верхнего меню</h1></td></tr>
                            </table>
                            <?php
                                $args = array( 'hide_empty' => false, 'orderby' => 'name' );
                                $menus = wp_get_nav_menus( $args );
                            ?>
                            <table class="theme-option-table table-block" >
                                <td>Левая половина меню
                                </td>
                                <td>
                                    <?php
                                    $args = array(
                                        'id' => 'menu_left_side',
                                        'lang' => array(
                                            //'title' => 'Страница блока:',
                                            'ru' => array(
                                                'element' => view_menu_select("theme_settings[menu_left_side]",$options['menu_left_side'])
                                            ),
                                            'ua' => array(
                                                'element' => view_menu_select("theme_settings[menu_left_side_ua]",$options['menu_left_side_ua'])
                                            ),
                                            'eng' => array(
                                                'element' => view_menu_select("theme_settings[menu_left_side_eng]",$options['menu_left_side_eng'])
                                            )
                                        )
                                    );
                                    echo view_lang_input($args);
                                    ?>
                                    <?php // echo view_menu_select("theme_settings[menu_right_side]",$options['menu_right_side']); ?>
                                </td>
                            </table>
                            <table class="theme-option-table table-block" >
                                <td>
                                    Правая половина меню
                                </td>
                                <td>
                                    <?php
                                    $args = array(
                                        'id' => 'menu_right_side',
                                        'lang' => array(
                                            //'title' => 'Страница блока:',
                                            'ru' => array(
                                                'element' => view_menu_select("theme_settings[menu_right_side]",$options['menu_right_side'])
                                            ),
                                            'ua' => array(
                                                'element' => view_menu_select("theme_settings[menu_right_side_ua]",$options['menu_right_side_ua'])
                                            ),
                                            'eng' => array(
                                                'element' => view_menu_select("theme_settings[menu_right_side_eng]",$options['menu_right_side_eng'])
                                            )
                                        )
                                    );
                                    echo view_lang_input($args);
                                    ?>
                                    <?php // echo view_menu_select("theme_settings[menu_right_side]",$options['menu_right_side']); ?>
                                </td>
                            </table>
                            <table class="theme-option-table">
                                <tr valign="top"><td colspan='2'><h1>Настройка блога</h1></td></tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <tr>
                                    <td><span>Навигация:</span></td>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'prev_post_link',
                                            'lang' => array(
                                                'title' => 'Предыдущая статья:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[prev_post_link]',
                                                    'value' => $options['prev_post_link']?$options['prev_post_link']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[prev_post_link_ua]',
                                                    'value' => $options['prev_post_link_ua']?$options['prev_post_link_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[prev_post_link_eng]',
                                                    'value' => $options['prev_post_link_eng']?$options['prev_post_link_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'next_post_link',
                                            'lang' => array(
                                                'title' => 'Следующая статья:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[next_post_link]',
                                                    'value' => $options['next_post_link']?$options['next_post_link']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[next_post_link_ua]',
                                                    'value' => $options['next_post_link_ua']?$options['next_post_link_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[next_post_link_eng]',
                                                    'value' => $options['next_post_link_eng']?$options['next_post_link_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span>Кнопка читать далее:</span></td>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'read_post_link',
                                            'lang' => array(
                                                //'title' => 'Предыдущая статья:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[read_post_link]',
                                                    'value' => $options['read_post_link']?$options['read_post_link']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[pread_post_link_ua]',
                                                    'value' => $options['read_post_link_ua']?$options['read_post_link_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[read_post_link_eng]',
                                                    'value' => $options['read_post_link_eng']?$options['read_post_link_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <tr>
                                    <td colspan="2">Настройка комментариев</td>
                                </tr>
                                <tr>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'comment_apply_button',
                                            'lang' => array(
                                                'title' => 'Кнопка оставить комментарий:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[comment_apply_button]',
                                                    'value' => $options['comment_apply_button']?$options['comment_apply_button']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[comment_apply_button_ua]',
                                                    'value' => $options['comment_apply_button_ua']?$options['comment_apply_button_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[comment_apply_button_eng]',
                                                    'value' => $options['comment_apply_button_eng']?$options['comment_apply_button_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'author_input_placeholder',
                                            'lang' => array(
                                                'title' => 'Поле автор:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[author_input_placeholder]',
                                                    'value' => $options['author_input_placeholder']?$options['author_input_placeholder']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[comment_apply_button_ua]',
                                                    'value' => $options['author_input_placeholder_ua']?$options['author_input_placeholder_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[author_input_placeholder_eng]',
                                                    'value' => $options['author_input_placeholder_eng']?$options['author_input_placeholder_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'comment_textarea_placeholder',
                                            'lang' => array(
                                                'title' => 'Поле комментарий:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[comment_textarea_placeholder]',
                                                    'value' => $options['comment_textarea_placeholder']?$options['comment_textarea_placeholder']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[comment_textarea_placeholder_ua]',
                                                    'value' => $options['comment_textarea_placeholder_ua']?$options['comment_textarea_placeholder_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[comment_textarea_placeholder_eng]',
                                                    'value' => $options['comment_textarea_placeholder_eng']?$options['comment_textarea_placeholder_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                            </table>


                            <table class="theme-option-table table-block" >
                                <tr>
                                    <td colspan="2">Настройка кнопок футера</td>
                                </tr>
                                <tr>

                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'footer_button',
                                            'lang' => array(
                                                'title' => 'Карьера у нас:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[footer_button]',
                                                    'value' => $options['footer_button']?$options['footer_button']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[footer_button_ua]',
                                                    'value' => $options['footer_button_ua']?$options['footer_button_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[footer_button_eng]',
                                                    'value' => $options['footer_button_eng']?$options['footer_button_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>

                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'footer_button2',
                                            'lang' => array(
                                                'title' => 'Партнерам:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[footer_button2]',
                                                    'value' => $options['footer_button2']?$options['footer_button2']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[footer_button2_ua]',
                                                    'value' => $options['footer_button2_ua']?$options['footer_button2_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[footer_button2_eng]',
                                                    'value' => $options['footer_button2_eng']?$options['footer_button2_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>

                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'footer_button3',
                                            'lang' => array(
                                                'title' => 'F.A.Q.:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[footer_button3]',
                                                    'value' => $options['footer_button3']?$options['footer_button3']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[footer_button3_ua]',
                                                    'value' => $options['footer_button3_ua']?$options['footer_button3_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[footer_button3_eng]',
                                                    'value' => $options['footer_button3_eng']?$options['footer_button3_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>

                                </tr>
                            </table>

                        </section>
                        <section id="content2">
                            <table class="theme-option-table table-block" >
                                <tr>
                                    <td>Адрес</td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'contacts_adres',
                                            'lang' => array(
                                                //'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[contacts_adres]',
                                                    'value' => $options['contacts_adres']?$options['contacts_adres']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[contacts_adres_ua]',
                                                    'value' => $options['contacts_adres_ua']?$options['contacts_adres_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[contacts_adres_eng]',
                                                    'value' => $options['contacts_adres_eng']?$options['contacts_adres_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <tr>
                                    <td>Карта Google maps</td>
                                    <td>Широта: <input type="text" name="theme_settings[maps_lat]" placeholder="Введите широту" value="<?php echo esc_attr(tf_variable_exist($options['maps_lat'])); ?>" style="width: 250px;" ></td>
                                    <td>Долгота: <input type="text" name="theme_settings[maps_lng]" placeholder="Введите долготу" value="<?php echo esc_attr(tf_variable_exist($options['maps_lng'])); ?>" style="width: 250px;" ></td>
                                    <td>Зум: <input type="text" name="theme_settings[maps_zoom]" placeholder="Зум" value="<?php echo esc_attr(tf_variable_exist($options['maps_zoom'])); ?>" style="width: 80px;" ></td>
                                    <td>Маркер <?php echo tf_img_upload('theme_settings[maps_marker]', tf_variable_exist($options['maps_marker']), 40); ?> </td>
                                </tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <tr>
                                    <td>Блок контакты</td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'contacts_info_block',
                                            'lang' => array(
                                                //'title' => 'Название блока:',
                                                'ru' => array(
                                                    'element_type' => 'textarea',
                                                    'name' => 'theme_settings[contacts_info_block]',
                                                    'value' => $options['contacts_info_block']?$options['contacts_info_block']:''
                                                ),
                                                'ua' => array(
                                                    'element_type' => 'textarea',
                                                    'name' => 'theme_settings[contacts_info_block_ua]',
                                                    'value' => $options['contacts_info_block_ua']?$options['contacts_info_block_ua']:''
                                                ),
                                                'eng' => array(
                                                    'element_type' => 'textarea',
                                                    'name' => 'theme_settings[contacts_info_block_eng]',
                                                    'value' => $options['contacts_info_block_eng']?$options['contacts_info_block_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <tr>
                                    <td>Запись по телефону</td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'contacts_call_record',
                                            'lang' => array(
                                                //'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[contacts_call_record]',
                                                    'value' => $options['contacts_call_record']?$options['contacts_call_record']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[contacts_call_record_ua]',
                                                    'value' => $options['contacts_call_record_ua']?$options['contacts_call_record_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[contacts_call_record_eng]',
                                                    'value' => $options['contacts_call_record_eng']?$options['contacts_call_record_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            <table class="theme-option-table table-block" >
                            <tr>
                                <td>График роботи</td>
                                <td>
                                <?php
                                $args = array(
                                    'id' => 'contacts_schedule',
                                    'lang' => array(
                                        //'title' => 'Название блока:',
                                        'ru' => array(
                                            'name' => 'theme_settings[contacts_schedule]',
                                            'value' => $options['contacts_schedule']?$options['contacts_schedule']:''
                                        ),
                                        'ua' => array(
                                            'name' => 'theme_settings[contacts_schedule_ua]',
                                            'value' => $options['contacts_schedule_ua']?$options['contacts_schedule_ua']:''
                                        ),
                                        'eng' => array(
                                            'name' => 'theme_settings[contacts_schedule_eng]',
                                            'value' => $options['contacts_schedule_eng']?$options['contacts_schedule_eng']:''
                                        )
                                    )
                                );
                                echo view_lang_input($args);
                                ?>
                                </td>
                            </tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <tr>
                                    <td>Електронный адрес</td>
                                    <td><input type="text" name="theme_settings[contacts_email]" value="<?php echo esc_attr(tf_variable_exist($options['contacts_email'])); ?>" style="width: 400px;" ></td>
                                </tr>
                            </table>
                            <table id="add-field-area" class="theme-option-table table-block" >
                                <thead>
                                <th colspan="2"><input type="text" id="phone_number" style="width: 300px;" placeholder="Номер телефона">
                                <input type="button" id="add_phone_button" value="Добавить телефон"></th>
                                </thead>
                                <?php
                                 if(tf_variable_exist($options['contacts_phone'])):
                                    foreach($options['contacts_phone'] as $key=>$phone):
                                ?>
                                        <tr id="ph<?php echo esc_html($key); ?>" class="phone">
                                        <td colspan="2"><input type="text" name="theme_settings[contacts_phone][]" value="<?php echo esc_html($phone); ?>" style="width: 300px;" placeholder="Номер телефона">
                                           <input type="button" id="delete-number" phone-id="<?php echo esc_html($key); ?>" value="Удалить телефон"></td>
                                        </tr>
                                <?php
                                    endforeach;
                                 endif;
                                ?>

                            </table>
                            <table class="theme-option-table">
                                <tr valign="top"><td colspan='2'><h1>Социальные сети</h1></td></tr>
                            </table>
                            <table class="theme-option-table table-block social" >
                                <thead>
                                <th>Название</th>
                                <th>Состояние </th>
                                <th>Ссылка</th>
                                </thead>
                                <?php
                                $social_vk_visible = isset($options['social_vk_visible'])?$options['social_vk_visible']:'';
                                $social_vk_url = isset($options['social_vk_url'])?$options['social_vk_url']:'';
                                $social_facebook_visible = isset($options['social_facebook_visible'])?$options['social_facebook_visible']:'';
                                $social_facebook_url = isset($options['social_facebook_url'])?$options['social_facebook_url']:'';
                                $social_instagram_visible = isset($options['social_instagram_visible'])?$options['social_instagram_visible']:'';
                                $social_instagram_url = isset($options['social_instagram_url'])?$options['social_instagram_url']:'';
                                $social_twitter_visible = isset($options['social_twitter_visible'])?$options['social_twitter_visible']:'';
                                $social_twitter_url = isset($options['social_twitter_url'])?$options['social_twitter_url']:'';
                                $social_youtube_url = isset($options['social_youtube_url'])?$options['social_youtube_url']:'';
                                $social_youtube_visible = isset($options['social_youtube_visible'])?$options['social_youtube_visible']:'';
                                $social_pinterest_url = isset($options['social_pinterest_url'])?$options['social_pinterest_url']:'';
                                $social_pinterest_visible = isset($options['social_pinterest_visible'])?$options['social_pinterest_visible']:'';
                                $social_periscope_url = isset($options['social_periscope_url'])?$options['social_periscope_url']:'';
                                $social_periscope_visible = isset($options['social_periscope_visible'])?$options['social_periscope_visible']:'';

                                ?>
                                <tr>
                                    <td>Вконтакте</td>
                                    <td><input type="checkbox" name="theme_settings[social_vk_visible]" value="1" <?php checked($social_vk_visible,'1'); ?>></td>
                                    <td><input type="text" name="theme_settings[social_vk_url]" value="<?php echo esc_attr($social_vk_url); ?>" style="width: 400px;" ></td>
                                </tr>
                                <tr>
                                    <td>Facebook</td>
                                    <td><input type="checkbox" name="theme_settings[social_facebook_visible]" value="1" <?php checked($social_facebook_visible,'1'); ?>></td>
                                    <td><input type="text" name="theme_settings[social_facebook_url]" value="<?php echo esc_attr($social_facebook_url); ?>" style="width: 400px;" ></td>
                                </tr>
                                <tr>
                                    <td>Instagram</td>
                                    <td><input type="checkbox" name="theme_settings[social_instagram_visible]" value="1" <?php checked($social_instagram_visible,'1'); ?>></td>
                                    <td><input type="text" name="theme_settings[social_instagram_url]" value="<?php echo esc_attr($social_instagram_url); ?>" style="width: 400px;" ></td>
                                </tr>
                                <tr>
                                    <td>Twitter</td>
                                    <td><input type="checkbox" name="theme_settings[social_twitter_visible]" value="1" <?php checked($social_twitter_visible,'1'); ?>></td>
                                    <td><input type="text" name="theme_settings[social_twitter_url]" value="<?php echo esc_attr($social_twitter_url); ?>" style="width: 400px;" ></td>
                                </tr>
                                <tr>
                                    <td>Youtube</td>
                                    <td><input type="checkbox" name="theme_settings[social_youtube_visible]" value="1" <?php checked($social_youtube_visible,'1'); ?>></td>
                                    <td><input type="text" name="theme_settings[social_youtube_url]" value="<?php echo esc_attr($social_youtube_url); ?>" style="width: 400px;" ></td>
                                </tr>
                                <tr>
                                    <td>Pinterest</td>
                                    <td><input type="checkbox" name="theme_settings[social_pinterest_visible]" value="1" <?php checked($social_pinterest_visible,'1'); ?>></td>
                                    <td><input type="text" name="theme_settings[social_pinterest_url]" value="<?php echo esc_attr($social_pinterest_url); ?>" style="width: 400px;" ></td>
                                </tr>
                                <tr>
                                    <td>Periscope</td>
                                    <td><input type="checkbox" name="theme_settings[social_periscope_visible]" value="1" <?php checked($social_periscope_visible,'1'); ?>></td>
                                    <td><input type="text" name="theme_settings[social_periscope_url]" value="<?php echo esc_attr($social_periscope_url); ?>" style="width: 400px;" ></td>
                                </tr>
                            </table>
                        </section>
                        <section id="content3">
                            <table class="theme-option-table">
                                <tr valign="top"><td colspan='2'><h1>Настройка блока главной страницы</h1></td></tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <tr>
                                    <td colspan="2">
                                        <h3>Заголовки блоков главной страницы:</h3>
                                        <div id="tabs2" class="tabs2">
                                            <ul>
                                                <li>Рус.</li>
                                                <li>Укр.</li>
                                                <li>Англ.</li>
                                            </ul>
                                            <div>
                                                <div>
                                                    <h3>О нас:</h3>
                                                    <input type="text" name="theme_settings[block_mp_title]" placeholder="Заголовок блока" value="<?php echo esc_attr($options['block_mp_title']?$options['block_mp_title']:''); ?>" style="width: 300px;" >

                                                </div>
                                                <div>
                                                    <h3>О нас:</h3>
                                                    <input type="text" name="theme_settings[block_mp_title_ua]" placeholder="Заголовок блока" value="<?php echo esc_attr($options['block_mp_title_ua']?$options['block_mp_title_ua']:''); ?>" style="width: 300px;" >

                                                </div>
                                                <div>
                                                    <h3>О нас:</h3>
                                                    <input type="text" name="theme_settings[block_mp_title_eng]" placeholder="Заголовок блока" value="<?php echo esc_attr($options['block_mp_title_eng']?$options['block_mp_title_eng']:''); ?>" style="width: 300px;" >

                                                </div>
                                            </div>
                                        </div>


                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px; ">
                                        <div class="main-container">
                                            <div class="left-block block-active">
                                            </div>
                                            <div class="right-block">
                                                <div class="own-block"></div>
                                                <div class="own-block"></div>
                                                <div class="own-block"></div>
                                                <div class="own-block"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="2">
                                        <?php
                                            $args = array(
                                                'id' => 'block_mp_1_title',
                                                'lang' => array(
                                                    'title' => 'Название блока:',
                                                    'ru' => array(
                                                        'name' => 'theme_settings[block_mp_1_title]',
                                                        'value' => $options['block_mp_1_title']?$options['block_mp_1_title']:''
                                                    ),
                                                    'ua' => array(
                                                        'name' => 'theme_settings[block_mp_1_title_ua]',
                                                        'value' => $options['block_mp_1_title_ua']?$options['block_mp_1_title_ua']:''
                                                    ),
                                                    'eng' => array(
                                                        'name' => 'theme_settings[block_mp_1_title_eng]',
                                                        'value' => $options['block_mp_1_title_eng']?$options['block_mp_1_title_eng']:''
                                                    )
                                                )
                                            );
                                            echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_mp_1_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_pages('theme_settings[block_mp_1_url]',$options['block_mp_1_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_pages('theme_settings[block_mp_1_url_ua]',$options['block_mp_1_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_pages('theme_settings[block_mp_1_url_eng]',$options['block_mp_1_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_mp_1_image]', tf_variable_exist($options['block_mp_1_image']));
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px; ">
                                        <div class="main-container">
                                            <div class="left-block">
                                            </div>
                                            <div class="right-block">
                                                <div class="own-block block-active"></div>
                                                <div class="own-block"></div>
                                                <div class="own-block"></div>
                                                <div class="own-block"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_mp_2_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_mp_2_title]',
                                                    'value' => $options['block_mp_2_title']?$options['block_mp_2_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_mp_2_title_ua]',
                                                    'value' => $options['block_mp_2_title_ua']?$options['block_mp_2_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_mp_1_title_eng]',
                                                    'value' => $options['block_mp_2_title_eng']?$options['block_mp_2_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_mp_2_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_pages('theme_settings[block_mp_2_url]',$options['block_mp_2_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_pages('theme_settings[block_mp_2_url_ua]',$options['block_mp_2_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_pages('theme_settings[block_mp_2_url_eng]',$options['block_mp_2_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_mp_2_image]', tf_variable_exist($options['block_mp_2_image']));
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px; ">
                                        <div class="main-container">
                                            <div class="left-block">
                                            </div>
                                            <div class="right-block">
                                                <div class="own-block"></div>
                                                <div class="own-block block-active"></div>
                                                <div class="own-block"></div>
                                                <div class="own-block"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_mp_3_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_mp_3_title]',
                                                    'value' => $options['block_mp_3_title']?$options['block_mp_3_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_mp_3_title_ua]',
                                                    'value' => $options['block_mp_3_title_ua']?$options['block_mp_3_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_mp_3_title_eng]',
                                                    'value' => $options['block_mp_3_title_eng']?$options['block_mp_3_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_mp_3_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_pages('theme_settings[block_mp_3_url]',$options['block_mp_3_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_pages('theme_settings[block_mp_3_url_ua]',$options['block_mp_3_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_pages('theme_settings[block_mp_3_url_eng]',$options['block_mp_3_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_mp_3_image]', tf_variable_exist($options['block_mp_3_image']));
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px; ">
                                        <div class="main-container">
                                            <div class="left-block">
                                            </div>
                                            <div class="right-block">
                                                <div class="own-block"></div>
                                                <div class="own-block"></div>
                                                <div class="own-block block-active"></div>
                                                <div class="own-block"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_mp_4_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_mp_4_title]',
                                                    'value' => $options['block_mp_4_title']?$options['block_mp_4_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_mp_4_title_ua]',
                                                    'value' => $options['block_mp_4_title_ua']?$options['block_mp_4_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_mp_4_title_eng]',
                                                    'value' => $options['block_mp_4_title_eng']?$options['block_mp_4_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_mp_4_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_pages('theme_settings[block_mp_4_url]',$options['block_mp_4_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_pages('theme_settings[block_mp_4_url_ua]',$options['block_mp_4_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_pages('theme_settings[block_mp_4_url_eng]',$options['block_mp_4_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_mp_4_image]', tf_variable_exist($options['block_mp_4_image']));
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px; ">
                                        <div class="main-container">
                                            <div class="left-block">
                                            </div>
                                            <div class="right-block">
                                                <div class="own-block"></div>
                                                <div class="own-block"></div>
                                                <div class="own-block"></div>
                                                <div class="own-block block-active"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_mp_5_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_mp_5_title]',
                                                    'value' => $options['block_mp_5_title']?$options['block_mp_5_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_mp_5_title_ua]',
                                                    'value' => $options['block_mp_5_title_ua']?$options['block_mp_5_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_mp_5_title_eng]',
                                                    'value' => $options['block_mp_5_title_eng']?$options['block_mp_5_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_mp_5_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_pages('theme_settings[block_mp_5_url]',$options['block_mp_1_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_pages('theme_settings[block_mp_5_url_ua]',$options['block_mp_1_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_pages('theme_settings[block_mp_5_url_eng]',$options['block_mp_1_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_mp_5_image]', tf_variable_exist($options['block_mp_5_image']));
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            <table class="theme-option-table">
                                <tr valign="top"><td colspan='2'><h1>Настройка страницы ЗМІ про нас</h1></td></tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <tr>
                                    <td>
                                        <h3>Выбор категории для отображения на странице ЗМІ про нас</h3>
                                        <select name="theme_settings[mass_media_about_us]" style="width:300px">
                                        <?php
                                        $args = array(
                                            'type'         => 'post',
                                            'child_of'     => 0,
                                            'parent'       => '',
                                            'orderby'      => 'name',
                                            'order'        => 'ASC',
                                            'hide_empty'   => 1,
                                            'hierarchical' => 1,
                                            'exclude'      => '',
                                            'include'      => '',
                                            'number'       => 0,
                                            'taxonomy'     => 'category',
                                            'pad_counts'   => false
                                        );

                                        $mass_media_about_us = isset($options['mass_media_about_us'])?$options['mass_media_about_us']:0;

                                        ?>
                                            <option value="0" <?php selected( $mass_media_about_us, '0' ) ?>>Не выбрано</option>
                                        <?php
                                            $categories = get_categories( $args );
                                            if( $categories ){
                                                foreach( $categories as $cat ){
                                                    ?>
                                                    <option value="<?php echo esc_attr($cat->slug); ?>" <?php selected( $mass_media_about_us, $cat->slug ) ?>><?php echo esc_html($cat->name) ?></option>
                                                <?php
                                               }
                                            }
                                        ?>
                                        </select>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'mass_media_about_us',
                                            'lang' => array(
                                                'title' => 'Выбор категории:',
                                                'ru' => array(
                                                    'element' => view_category_select('theme_settings[mass_media_about_us]',$options['mass_media_about_us'])
                                                ),
                                                'ua' => array(
                                                    'element' => view_category_select('theme_settings[mass_media_about_us_ua]',$options['mass_media_about_us_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => view_category_select('theme_settings[mass_media_about_us_eng]',$options['mass_media_about_us_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            <!-- Настройка блока услуг -->
                            <table class="theme-option-table">
                                <tr valign="top"><td colspan='2'><h1>Настройка страницы услуги</h1></td></tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <tr>
                                    <td>
                                        <div class="services-conteiner">
                                            <div class="services-right">
                                                <div class="services-full-block services-margin-bottom block-active"></div>
                                                <div class="services-mini-block"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                            </div>
                                            <div class="services-right">
                                                <div class="services-mini-block services-margin-bottom"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                                <div class="services-full-block"></div>
                                            </div>
                                            <div class="services-full">
                                                <div class="services-mini-2"></div>
                                                <div class="services-full-block-2"></div>
                                                <div class="services-mini-2-2"></div>
                                                <div class="services-mini-2-2"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                        $block_services_1_title = isset($options['block_services_1_title'])?$options['block_services_1_title']:'';
                                        $block_services_1_url   = isset($options['block_services_1_url'])?$options['block_services_1_url']:'';
                                        $block_services_1_img   = isset($options['block_services_1_img'])?$options['block_services_1_img']:'';
                                    ?>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_1_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_services_1_title]',
                                                    'value' => $options['block_services_1_title']?$options['block_services_1_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_services_1_title_ua]',
                                                    'value' => $options['block_services_1_title_ua']?$options['block_services_1_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_services_1_title_eng]',
                                                    'value' => $options['block_services_1_title_eng']?$options['block_services_1_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_1_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_1_url]',$options['block_services_1_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_1_url_ua]',$options['block_services_1_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_1_url_eng]',$options['block_services_1_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_services_1_img]', $block_services_1_img);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="services-conteiner">
                                            <div class="services-right">
                                                <div class="services-full-block services-margin-bottom"></div>
                                                <div class="services-mini-block"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                            </div>
                                            <div class="services-right">
                                                <div class="services-mini-block services-margin-bottom block-active"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                                <div class="services-full-block"></div>
                                            </div>
                                            <div class="services-full">
                                                <div class="services-mini-2"></div>
                                                <div class="services-full-block-2"></div>
                                                <div class="services-mini-2-2"></div>
                                                <div class="services-mini-2-2"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                    $block_services_2_title = isset($options['block_services_2_title'])?$options['block_services_2_title']:'';
                                    $block_services_2_url   = isset($options['block_services_2_url'])?$options['block_services_2_url']:'';
                                    $block_services_2_img   = isset($options['block_services_2_img'])?$options['block_services_2_img']:'';
                                    ?>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_2_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_services_2_title]',
                                                    'value' => $options['block_services_2_title']?$options['block_services_2_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_services_2_title_ua]',
                                                    'value' => $options['block_services_2_title_ua']?$options['block_services_2_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_services_2_title_eng]',
                                                    'value' => $options['block_services_2_title_eng']?$options['block_services_2_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_2_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_2_url]',$options['block_services_2_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_2_url_ua]',$options['block_services_2_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_2_url_eng]',$options['block_services_2_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_services_2_img]', $block_services_2_img);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="services-conteiner">
                                            <div class="services-right">
                                                <div class="services-full-block services-margin-bottom"></div>
                                                <div class="services-mini-block"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                            </div>
                                            <div class="services-right">
                                                <div class="services-mini-block services-margin-bottom"></div>
                                                <div class="services-mini-block non-margin-right block-active"></div>
                                                <div class="services-full-block"></div>
                                            </div>
                                            <div class="services-full">
                                                <div class="services-mini-2"></div>
                                                <div class="services-full-block-2"></div>
                                                <div class="services-mini-2-2"></div>
                                                <div class="services-mini-2-2"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                    $block_services_3_title = isset($options['block_services_3_title'])?$options['block_services_3_title']:'';
                                    $block_services_3_url   = isset($options['block_services_3_url'])?$options['block_services_3_url']:'';
                                    $block_services_3_img   = isset($options['block_services_3_img'])?$options['block_services_3_img']:'';
                                    ?>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_3_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_services_3_title]',
                                                    'value' => $options['block_services_3_title']?$options['block_services_3_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_services_3_title_ua]',
                                                    'value' => $options['block_services_3_title_ua']?$options['block_services_3_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_services_3_title_eng]',
                                                    'value' => $options['block_services_3_title_eng']?$options['block_services_3_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_3_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_3_url]',$options['block_services_3_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_3_url_ua]',$options['block_services_3_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_3_url_eng]',$options['block_services_3_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_services_3_img]', $block_services_3_img);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="services-conteiner">
                                            <div class="services-right">
                                                <div class="services-full-block services-margin-bottom"></div>
                                                <div class="services-mini-block"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                            </div>
                                            <div class="services-right">
                                                <div class="services-mini-block services-margin-bottom"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                                <div class="services-full-block block-active"></div>
                                            </div>
                                            <div class="services-full">
                                                <div class="services-mini-2"></div>
                                                <div class="services-full-block-2"></div>
                                                <div class="services-mini-2-2"></div>
                                                <div class="services-mini-2-2"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                    $block_services_4_title = isset($options['block_services_4_title'])?$options['block_services_4_title']:'';
                                    $block_services_4_url   = isset($options['block_services_4_url'])?$options['block_services_4_url']:'';
                                    $block_services_4_img   = isset($options['block_services_4_img'])?$options['block_services_4_img']:'';
                                    ?>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_4_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_services_4_title]',
                                                    'value' => $options['block_services_4_title']?$options['block_services_4_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_services_4_title_ua]',
                                                    'value' => $options['block_services_4_title_ua']?$options['block_services_4_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_services_4_title_eng]',
                                                    'value' => $options['block_services_4_title_eng']?$options['block_services_4_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_4_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_4_url]',$options['block_services_4_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_4_url_ua]',$options['block_services_4_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_4_url_eng]',$options['block_services_4_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_services_4_img]', $block_services_4_img);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="services-conteiner">
                                            <div class="services-right">
                                                <div class="services-full-block services-margin-bottom"></div>
                                                <div class="services-mini-block block-active"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                            </div>
                                            <div class="services-right">
                                                <div class="services-mini-block services-margin-bottom"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                                <div class="services-full-block"></div>
                                            </div>
                                            <div class="services-full">
                                                <div class="services-mini-2"></div>
                                                <div class="services-full-block-2"></div>
                                                <div class="services-mini-2-2"></div>
                                                <div class="services-mini-2-2"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                    $block_services_5_title = isset($options['block_services_5_title'])?$options['block_services_5_title']:'';
                                    $block_services_5_url   = isset($options['block_services_5_url'])?$options['block_services_5_url']:'';
                                    $block_services_5_img   = isset($options['block_services_5_img'])?$options['block_services_5_img']:'';
                                    ?>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_5_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_services_5_title]',
                                                    'value' => $options['block_services_5_title']?$options['block_services_5_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_services_5_title_ua]',
                                                    'value' => $options['block_services_5_title_ua']?$options['block_services_5_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_services_5_title_eng]',
                                                    'value' => $options['block_services_5_title_eng']?$options['block_services_5_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_5_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_5_url]',$options['block_services_5_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_5_url_ua]',$options['block_services_5_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_5_url_eng]',$options['block_services_5_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_services_5_img]', $block_services_5_img);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="services-conteiner">
                                            <div class="services-right">
                                                <div class="services-full-block services-margin-bottom"></div>
                                                <div class="services-mini-block"></div>
                                                <div class="services-mini-block non-margin-right block-active"></div>
                                            </div>
                                            <div class="services-right">
                                                <div class="services-mini-block services-margin-bottom"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                                <div class="services-full-block"></div>
                                            </div>
                                            <div class="services-full">
                                                <div class="services-mini-2"></div>
                                                <div class="services-full-block-2"></div>
                                                <div class="services-mini-2-2"></div>
                                                <div class="services-mini-2-2"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                    $block_services_6_title = isset($options['block_services_6_title'])?$options['block_services_6_title']:'';
                                    $block_services_6_url   = isset($options['block_services_6_url'])?$options['block_services_6_url']:'';
                                    $block_services_6_img   = isset($options['block_services_6_img'])?$options['block_services_6_img']:'';
                                    ?>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_6_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_services_6_title]',
                                                    'value' => $options['block_services_6_title']?$options['block_services_6_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_services_6_title_ua]',
                                                    'value' => $options['block_services_6_title_ua']?$options['block_services_6_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_services_6_title_eng]',
                                                    'value' => $options['block_services_6_title_eng']?$options['block_services_6_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_6_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_6_url]',$options['block_services_6_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_6_url_ua]',$options['block_services_6_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_6_url_eng]',$options['block_services_6_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_services_6_img]', $block_services_6_img);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="services-conteiner">
                                            <div class="services-right">
                                                <div class="services-full-block services-margin-bottom"></div>
                                                <div class="services-mini-block"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                            </div>
                                            <div class="services-right">
                                                <div class="services-mini-block services-margin-bottom"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                                <div class="services-full-block"></div>
                                            </div>
                                            <div class="services-full">
                                                <div class="services-mini-2 block-active"></div>
                                                <div class="services-full-block-2"></div>
                                                <div class="services-mini-2-2"></div>
                                                <div class="services-mini-2-2"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                    $block_services_7_title = isset($options['block_services_7_title'])?$options['block_services_7_title']:'';
                                    $block_services_7_url   = isset($options['block_services_7_url'])?$options['block_services_7_url']:'';
                                    $block_services_7_img   = isset($options['block_services_7_img'])?$options['block_services_7_img']:'';

                                    ?>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_7_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_services_7_title]',
                                                    'value' => $options['block_services_7_title']?$options['block_services_7_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_services_7_title_ua]',
                                                    'value' => $options['block_services_7_title_ua']?$options['block_services_7_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_services_7_title_eng]',
                                                    'value' => $options['block_services_7_title_eng']?$options['block_services_7_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_7_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_7_url]',$options['block_services_7_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_7_url_ua]',$options['block_services_7_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_7_url_eng]',$options['block_services_7_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_services_7_img]', $block_services_7_img);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="services-conteiner">
                                            <div class="services-right">
                                                <div class="services-full-block services-margin-bottom"></div>
                                                <div class="services-mini-block"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                            </div>
                                            <div class="services-right">
                                                <div class="services-mini-block services-margin-bottom"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                                <div class="services-full-block"></div>
                                            </div>
                                            <div class="services-full">
                                                <div class="services-mini-2"></div>
                                                <div class="services-full-block-2 block-active"></div>
                                                <div class="services-mini-2-2"></div>
                                                <div class="services-mini-2-2"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                    $block_services_8_title = isset($options['block_services_8_title'])?$options['block_services_8_title']:'';
                                    $block_services_8_url   = isset($options['block_services_8_url'])?$options['block_services_8_url']:'';
                                    $block_services_8_img   = isset($options['block_services_8_img'])?$options['block_services_8_img']:'';
                                    ?>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_8_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_services_8_title]',
                                                    'value' => $options['block_services_8_title']?$options['block_services_8_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_services_8_title_ua]',
                                                    'value' => $options['block_services_8_title_ua']?$options['block_services_8_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_services_8_title_eng]',
                                                    'value' => $options['block_services_8_title_eng']?$options['block_services_8_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_8_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_8_url]',$options['block_services_8_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_8_url_ua]',$options['block_services_8_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_8_url_eng]',$options['block_services_8_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_services_8_img]', $block_services_8_img);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="services-conteiner">
                                            <div class="services-right">
                                                <div class="services-full-block services-margin-bottom"></div>
                                                <div class="services-mini-block"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                            </div>
                                            <div class="services-right">
                                                <div class="services-mini-block services-margin-bottom"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                                <div class="services-full-block"></div>
                                            </div>
                                            <div class="services-full">
                                                <div class="services-mini-2"></div>
                                                <div class="services-full-block-2"></div>
                                                <div class="services-mini-2-2 block-active"></div>
                                                <div class="services-mini-2-2"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                    $block_services_9_title = isset($options['block_services_9_title'])?$options['block_services_9_title']:'';
                                    $block_services_9_url   = isset($options['block_services_9_url'])?$options['block_services_9_url']:'';
                                    $block_services_9_img   = isset($options['block_services_9_img'])?$options['block_services_9_img']:'';
                                    ?>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_9_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_services_9_title]',
                                                    'value' => $options['block_services_9_title']?$options['block_services_9_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_services_9_title_ua]',
                                                    'value' => $options['block_services_9_title_ua']?$options['block_services_9_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_services_9_title_eng]',
                                                    'value' => $options['block_services_9_title_eng']?$options['block_services_9_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_9_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_9_url]',$options['block_services_9_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_9_url_ua]',$options['block_services_9_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_9_url_eng]',$options['block_services_9_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_services_9_img]', $block_services_9_img);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="services-conteiner">
                                            <div class="services-right">
                                                <div class="services-full-block services-margin-bottom"></div>
                                                <div class="services-mini-block"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                            </div>
                                            <div class="services-right">
                                                <div class="services-mini-block services-margin-bottom"></div>
                                                <div class="services-mini-block non-margin-right"></div>
                                                <div class="services-full-block"></div>
                                            </div>
                                            <div class="services-full">
                                                <div class="services-mini-2"></div>
                                                <div class="services-full-block-2"></div>
                                                <div class="services-mini-2-2"></div>
                                                <div class="services-mini-2-2 block-active"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                    $block_services_10_title = isset($options['block_services_10_title'])?$options['block_services_10_title']:'';
                                    $block_services_10_url   = isset($options['block_services_10_url'])?$options['block_services_10_url']:'';
                                    $block_services_10_img   = isset($options['block_services_10_img'])?$options['block_services_10_img']:'';
                                    ?>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_10_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_services_10_title]',
                                                    'value' => $options['block_services_10_title']?$options['block_services_10_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_services_10_title_ua]',
                                                    'value' => $options['block_services_10_title_ua']?$options['block_services_10_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_services_10_title_eng]',
                                                    'value' => $options['block_services_10_title_eng']?$options['block_services_10_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_services_10_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_10_url]',$options['block_services_10_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_10_url_ua]',$options['block_services_10_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_pages('theme_settings[block_services_10_url_eng]',$options['block_services_10_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_services_10_img]', $block_services_10_img);
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            <table class="theme-option-table">
                                <tr valign="top"><td colspan='2'><h1>Настройка страницы магазина</h1></td></tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <tr>
                                    <td>
                                        <div class="services-conteiner">
                                            <div class="services-full">
                                                <div class="services-mini-2 block-active"></div>
                                                <div class="services-full-block-2"></div>
                                                <div class="services-mini-2-2"></div>
                                                <div class="services-mini-2-2"></div>
                                            </div>
                                            <div class="shop-block">
                                                <div class="shop-mini-block"></div>
                                                <div class="shop-mini-block"></div>
                                                <div class="shop-mini-double"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                    $block_shop_1_title = isset($options['block_shop_1_title'])?$options['block_shop_1_title']:'';
                                    $block_shop_1_url   = isset($options['block_shop_1_url'])?$options['block_shop_1_url']:'';
                                    $block_shop_1_img   = isset($options['block_shop_1_img'])?$options['block_shop_1_img']:'';
                                    ?>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_shop_1_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_shop_1_title]',
                                                    'value' => $options['block_shop_1_title']?$options['block_shop_1_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_shop_1_title_ua]',
                                                    'value' => $options['block_shop_1_title_ua']?$options['block_shop_1_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_shop_1_title_eng]',
                                                    'value' => $options['block_shop_1_title_eng']?$options['block_shop_1_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_shop_1_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_1_url]',$options['block_shop_1_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_1_url_ua]',$options['block_shop_1_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_1_url_eng]',$options['block_shop_1_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_shop_1_img]', $block_shop_1_img);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="services-conteiner">
                                            <div class="services-full">
                                                <div class="services-mini-2"></div>
                                                <div class="services-full-block-2 block-active"></div>
                                                <div class="services-mini-2-2"></div>
                                                <div class="services-mini-2-2"></div>
                                            </div>
                                            <div class="shop-block">
                                                <div class="shop-mini-block"></div>
                                                <div class="shop-mini-block"></div>
                                                <div class="shop-mini-double"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                    $block_shop_2_title = isset($options['block_shop_2_title'])?$options['block_shop_2_title']:'';
                                    $block_shop_2_url   = isset($options['block_shop_2_url'])?$options['block_shop_2_url']:'';
                                    $block_shop_2_img   = isset($options['block_shop_2_img'])?$options['block_shop_2_img']:'';
                                    ?>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_shop_2_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_shop_2_title]',
                                                    'value' => $options['block_shop_2_title']?$options['block_shop_2_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_shop_2_title_ua]',
                                                    'value' => $options['block_shop_2_title_ua']?$options['block_shop_2_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_shop_2_title_eng]',
                                                    'value' => $options['block_shop_2_title_eng']?$options['block_shop_2_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_shop_2_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_2_url]',$options['block_shop_2_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_2_url_ua]',$options['block_shop_2_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_2_url_eng]',$options['block_shop_2_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_shop_2_img]', $block_shop_2_img);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="services-conteiner">
                                            <div class="services-full">
                                                <div class="services-mini-2"></div>
                                                <div class="services-full-block-2"></div>
                                                <div class="services-mini-2-2 block-active"></div>
                                                <div class="services-mini-2-2"></div>
                                            </div>
                                            <div class="shop-block">
                                                <div class="shop-mini-block"></div>
                                                <div class="shop-mini-block"></div>
                                                <div class="shop-mini-double"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                    $block_shop_3_title = isset($options['block_shop_3_title'])?$options['block_shop_3_title']:'';
                                    $block_shop_3_url   = isset($options['block_shop_3_url'])?$options['block_shop_3_url']:'';
                                    $block_shop_3_img   = isset($options['block_shop_3_img'])?$options['block_shop_3_img']:'';
                                    ?>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_shop_3_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_shop_3_title]',
                                                    'value' => $options['block_shop_3_title']?$options['block_shop_3_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_shop_3_title_ua]',
                                                    'value' => $options['block_shop_3_title_ua']?$options['block_shop_3_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_shop_3_title_eng]',
                                                    'value' => $options['block_shop_3_title_eng']?$options['block_shop_3_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_shop_3_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_3_url]',$options['block_shop_3_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_3_url_ua]',$options['block_shop_3_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_3_url_eng]',$options['block_shop_3_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_shop_3_img]', $block_shop_3_img);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="services-conteiner">
                                            <div class="services-full">
                                                <div class="services-mini-2"></div>
                                                <div class="services-full-block-2"></div>
                                                <div class="services-mini-2-2"></div>
                                                <div class="services-mini-2-2 block-active"></div>
                                            </div>
                                            <div class="shop-block">
                                                <div class="shop-mini-block"></div>
                                                <div class="shop-mini-block"></div>
                                                <div class="shop-mini-double"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                    $block_shop_4_title = isset($options['block_shop_4_title'])?$options['block_shop_4_title']:'';
                                    $block_shop_4_url   = isset($options['block_shop_4_url'])?$options['block_shop_4_url']:'';
                                    $block_shop_4_img   = isset($options['block_shop_4_img'])?$options['block_shop_4_img']:'';
                                    ?>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_shop_4_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_shop_4_title]',
                                                    'value' => $options['block_shop_4_title']?$options['block_shop_4_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_shop_4_title_ua]',
                                                    'value' => $options['block_shop_4_title_ua']?$options['block_shop_4_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_shop_4_title_eng]',
                                                    'value' => $options['block_shop_4_title_eng']?$options['block_shop_4_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_shop_4_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_4_url]',$options['block_shop_4_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_4_url_ua]',$options['block_shop_4_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_4_url_eng]',$options['block_shop_4_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_shop_4_img]', $block_shop_4_img);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="services-conteiner">
                                            <div class="services-full">
                                                <div class="services-mini-2"></div>
                                                <div class="services-full-block-2"></div>
                                                <div class="services-mini-2-2"></div>
                                                <div class="services-mini-2-2"></div>
                                            </div>
                                            <div class="shop-block">
                                                <div class="shop-mini-block block-active"></div>
                                                <div class="shop-mini-block"></div>
                                                <div class="shop-mini-double"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                    $block_shop_5_title = isset($options['block_shop_5_title'])?$options['block_shop_5_title']:'';
                                    $block_shop_5_url   = isset($options['block_shop_5_url'])?$options['block_shop_5_url']:'';
                                    $block_shop_5_img   = isset($options['block_shop_5_img'])?$options['block_shop_5_img']:'';
                                    ?>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_shop_5_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_shop_5_title]',
                                                    'value' => $options['block_shop_5_title']?$options['block_shop_5_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_shop_5_title_ua]',
                                                    'value' => $options['block_shop_5_title_ua']?$options['block_shop_5_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_shop_5_title_eng]',
                                                    'value' => $options['block_shop_5_title_eng']?$options['block_shop_5_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_shop_5_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_5_url]',$options['block_shop_5_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_5_url_ua]',$options['block_shop_5_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_5_url_eng]',$options['block_shop_5 _url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_shop_5_img]', $block_shop_5_img);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="services-conteiner">
                                            <div class="services-full">
                                                <div class="services-mini-2"></div>
                                                <div class="services-full-block-2"></div>
                                                <div class="services-mini-2-2"></div>
                                                <div class="services-mini-2-2"></div>
                                            </div>
                                            <div class="shop-block">
                                                <div class="shop-mini-block"></div>
                                                <div class="shop-mini-block block-active"></div>
                                                <div class="shop-mini-double"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                    $block_shop_6_title = isset($options['block_shop_6_title'])?$options['block_shop_6_title']:'';
                                    $block_shop_6_url   = isset($options['block_shop_6_url'])?$options['block_shop_6_url']:'';
                                    $block_shop_6_img   = isset($options['block_shop_6_img'])?$options['block_shop_6_img']:'';
                                    ?>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_shop_6_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_shop_6_title]',
                                                    'value' => $options['block_shop_6_title']?$options['block_shop_6_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_shop_6_title_ua]',
                                                    'value' => $options['block_shop_6_title_ua']?$options['block_shop_6_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_shop_6_title_eng]',
                                                    'value' => $options['block_shop_6_title_eng']?$options['block_shop_6_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_shop_6_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_6_url]',$options['block_shop_6_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_6_url_ua]',$options['block_shop_6_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_6_url_eng]',$options['block_shop_6_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_shop_6_img]', $block_shop_6_img);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="services-conteiner">
                                            <div class="services-full">
                                                <div class="services-mini-2"></div>
                                                <div class="services-full-block-2"></div>
                                                <div class="services-mini-2-2"></div>
                                                <div class="services-mini-2-2"></div>
                                            </div>
                                            <div class="shop-block">
                                                <div class="shop-mini-block"></div>
                                                <div class="shop-mini-block"></div>
                                                <div class="shop-mini-double block-active"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                    $block_shop_7_title = isset($options['block_shop_7_title'])?$options['block_shop_7_title']:'';
                                    $block_shop_7_url   = isset($options['block_shop_7_url'])?$options['block_shop_7_url']:'';
                                    $block_shop_7_img   = isset($options['block_shop_7_img'])?$options['block_shop_7_img']:'';
                                    ?>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'block_shop_7_title',
                                            'lang' => array(
                                                'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[block_shop_7_title]',
                                                    'value' => $options['block_shop_7_title']?$options['block_shop_7_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[block_shop_7_title_ua]',
                                                    'value' => $options['block_shop_7_title_ua']?$options['block_shop_7_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[block_shop_7_title_eng]',
                                                    'value' => $options['block_shop_7_title_eng']?$options['block_shop_7_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'block_shop_7_url',
                                            'lang' => array(
                                                'title' => 'Страница блока:',
                                                'ru' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_7_url]',$options['block_shop_7_url'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_7_url_ua]',$options['block_shop_7_url_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_woo_cat('theme_settings[block_shop_7_url_eng]',$options['block_shop_7_url_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <h3>Картинка фона:</h3>
                                        <?php
                                        echo tf_img_upload('theme_settings[block_shop_7_img]', $block_shop_7_img);
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            <!-- Настройка блока услуг -->
                            <table class="theme-option-table">
                                <tr valign="top"><td colspan='2'><h1>Настройка страницы школа барберів і тату</h1></td></tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <tr valign="top">
                                    <?php
                                        $school_top_block_id = isset($options['school_top_block_id'])?$options['school_top_block_id']:'';
                                        $school_bottom_block_id = isset($options['school_bottom_block_id'])?$options['school_bottom_block_id']:'';
                                    ?>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'school_top_block_id',
                                            'lang' => array(
                                                'title' => 'Страница верхнего блока:',
                                                'ru' => array(
                                                    'element' => tf_view_pages_with_id('theme_settings[school_top_block_id]',$options['school_top_block_id'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_pages_with_id('theme_settings[school_top_block_id_ua]',$options['school_top_block_id_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_pages_with_id('theme_settings[school_top_block_id_eng]',$options['school_top_block_id_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $args = array(
                                            'id' => 'school_bottom_block_id',
                                            'lang' => array(
                                                'title' => 'Страница нижнего блока:',
                                                'ru' => array(
                                                    'element' => tf_view_pages_with_id('theme_settings[school_bottom_block_id]',$options['school_bottom_block_id'])
                                                ),
                                                'ua' => array(
                                                    'element' => tf_view_pages_with_id('theme_settings[school_bottom_block_id_ua]',$options['school_bottom_block_id_ua'])
                                                ),
                                                'eng' => array(
                                                    'element' => tf_view_pages_with_id('theme_settings[school_bottom_block_id_eng]',$options['school_bottom_block_id_eng'])
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                            </table>

                        </section>
                        <section id="content4">
                            <table class="theme-option-table">
                                <tr valign="top"><td colspan='2'><h1>Блок мастеров</h1></td></tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <tr>
                                    <td><span>Заголовки блоков:</span></td>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'barber_master_block_title',
                                            'lang' => array(
                                                'title' => 'Барбер мастера:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[barber_master_block_title]',
                                                    'value' => $options['barber_master_block_title']?$options['barber_master_block_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[barber_master_block_title_ua]',
                                                    'value' => $options['barber_master_block_title_ua']?$options['barber_master_block_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[barber_master_block_title_eng]',
                                                    'value' => $options['barber_master_block_title_eng']?$options['barber_master_block_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'tattoo_master_block_title',
                                            'lang' => array(
                                                'title' => 'Тату мастера:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[tattoo_master_block_title]',
                                                    'value' => $options['tattoo_master_block_title']?$options['tattoo_master_block_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[tattoo_master_block_title_ua]',
                                                    'value' => $options['tattoo_master_block_title_ua']?$options['tattoo_master_block_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[tattoo_master_block_title_eng]',
                                                    'value' => $options['tattoo_master_block_title_eng']?$options['tattoo_master_block_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span>Тип мастера:</span></td>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'barber_master_single_type',
                                            'lang' => array(
                                                'title' => 'Барбер мастер:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[barber_master_single_type]',
                                                    'value' => $options['barber_master_single_type']?$options['barber_master_single_type']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[barber_master_single_type_ua]',
                                                    'value' => $options['barber_master_single_type_ua']?$options['barber_master_single_type_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[barber_master_single_type_eng]',
                                                    'value' => $options['barber_master_single_type_eng']?$options['barber_master_single_type_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'tattoo_master_single_type',
                                            'lang' => array(
                                                'title' => 'Тату мастер:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[tattoo_master_single_type]',
                                                    'value' => $options['tattoo_master_single_type']?$options['tattoo_master_single_type']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[tattoo_master_single_type_ua]',
                                                    'value' => $options['tattoo_master_single_type_ua']?$options['tattoo_master_single_type_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[tattoo_master_single_type_eng]',
                                                    'value' => $options['tattoo_master_single_type_eng']?$options['tattoo_master_single_type_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span>Текст блока работы мастера:</span></td>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'master_work_block_title',
                                            'lang' => array(
                                                //'title' => 'Тату мастера:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[master_work_block_title]',
                                                    'value' => $options['master_work_block_title']?$options['master_work_block_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[master_work_block_title_ua]',
                                                    'value' => $options['master_work_block_title_ua']?$options['master_work_block_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[master_work_block_title_eng]',
                                                    'value' => $options['master_work_block_title_eng']?$options['master_work_block_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span>Текст кнопки просмотреть профиль:</span></td>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'master_view_button',
                                            'lang' => array(
                                                //'title' => 'Тату мастера:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[master_view_button]',
                                                    'value' => $options['master_view_button']?$options['master_view_button']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[master_view_button_ua]',
                                                    'value' => $options['master_view_button_ua']?$options['master_view_button_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[master_view_button_eng]',
                                                    'value' => $options['master_view_button_eng']?$options['master_view_button_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <tr>
                                    <td><span>Заголовок блока услуги:</span></td>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'services_block_title',
                                            'lang' => array(
                                                'title' => 'Заголовок блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[services_block_title]',
                                                    'value' => $options['services_block_title']?$options['services_block_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[services_block_title_ua]',
                                                    'value' => $options['services_block_title_ua']?$options['services_block_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[services_block_title_eng]',
                                                    'value' => $options['services_block_title_eng']?$options['services_block_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <tr>
                                    <td><span>Заголовок блока контакты:</span></td>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'contacts_block_title',
                                            'lang' => array(
                                                'title' => 'Заголовок блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[contacts_block_title]',
                                                    'value' => $options['contacts_block_title']?$options['contacts_block_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[contacts_block_title_ua]',
                                                    'value' => $options['contacts_block_title_ua']?$options['contacts_block_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[contacts_block_title_eng]',
                                                    'value' => $options['contacts_block_title_eng']?$options['contacts_block_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <tr>
                                    <td><span>Текст кнопки запись онлайн:</span></td>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'button_check_online',
                                            'lang' => array(
                                                //'title' => 'Название блока:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[button_check_online]',
                                                    'value' => $options['button_check_online']?$options['button_check_online']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[button_check_online_ua]',
                                                    'value' => $options['button_check_online_ua']?$options['button_check_online_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[button_check_online_eng]',
                                                    'value' => $options['button_check_online_eng']?$options['button_check_online_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            <table class="theme-option-table">
                                <tr valign="top"><td colspan='2'><h1>Настройки магазина</h1></td></tr>
                            </table>
                            <table class="theme-option-table table-block" >
                                <tr>
                                    <td>Настройки виджета корзины</td>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'cart_widget_product_text',
                                            'lang' => array(
                                                'title' => 'Надпись товаров:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[cart_widget_product_text]',
                                                    'value' => $options['cart_widget_product_text']?$options['cart_widget_product_text']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[cart_widget_product_text_ua]',
                                                    'value' => $options['cart_widget_product_text_ua']?$options['cart_widget_product_text_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[cart_widget_product_text_eng]',
                                                    'value' => $options['cart_widget_product_text_eng']?$options['cart_widget_product_text_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'cart_widget_total_text',
                                            'lang' => array(
                                                'title' => 'Надпись сумма:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[cart_widget_total_text]',
                                                    'value' => $options['cart_widget_total_text']?$options['cart_widget_total_text']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[cart_widget_total_text_ua]',
                                                    'value' => $options['cart_widget_total_text_ua']?$options['cart_widget_total_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[cart_widget_total_text_eng]',
                                                    'value' => $options['cart_widget_total_text_eng']?$options['cart_widget_total_text_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Заголовок категорий товаров</td>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'shop_category_title',
                                            'lang' => array(
                                                'title' => 'Категории:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[shop_category_title]',
                                                    'value' => $options['shop_category_title']?$options['shop_category_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[shop_category_title_ua]',
                                                    'value' => $options['shop_category_title_ua']?$options['shop_category_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[shop_category_title_eng]',
                                                    'value' => $options['shop_category_title_eng']?$options['shop_category_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <?php
                                        $args = array(
                                            'id' => 'add_to_cart_button_text',
                                            'lang' => array(
                                                'title' => 'Кнопка добавить в корзину:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[add_to_cart_button_text]',
                                                    'value' => $options['add_to_cart_button_text']?$options['add_to_cart_button_text']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[button_check_online_ua]',
                                                    'value' => $options['add_to_cart_button_text_ua']?$options['add_to_cart_button_text_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[add_to_cart_button_text_eng]',
                                                    'value' => $options['add_to_cart_button_text_eng']?$options['add_to_cart_button_text_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3"><h2>Настройки корзины</h2></td>
                                </tr>
                                <tr>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'cart_table_cell_product',
                                            'lang' => array(
                                                'title' => 'Колонка товар:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[cart_table_cell_product]',
                                                    'value' => $options['cart_table_cell_product']?$options['cart_table_cell_product']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[cart_table_cell_product_ua]',
                                                    'value' => $options['cart_table_cell_product_ua']?$options['cart_table_cell_product_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[cart_table_cell_product_eng]',
                                                    'value' => $options['cart_table_cell_product_eng']?$options['cart_table_cell_product_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'cart_table_cell_price',
                                            'lang' => array(
                                                'title' => 'Колонка цена:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[cart_table_cell_price]',
                                                    'value' => $options['cart_table_cell_price']?$options['cart_table_cell_price']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[cart_table_cell_price_ua]',
                                                    'value' => $options['cart_table_cell_price_ua']?$options['cart_table_cell_price_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[cart_table_cell_price_eng]',
                                                    'value' => $options['cart_table_cell_price_eng']?$options['cart_table_cell_price_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'cart_table_cell_count',
                                            'lang' => array(
                                                'title' => 'Колонка количество:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[cart_table_cell_count]',
                                                    'value' => $options['cart_table_cell_count']?$options['cart_table_cell_count']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[cart_table_cell_count_ua]',
                                                    'value' => $options['cart_table_cell_count_ua']?$options['cart_table_cell_count_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[cart_table_cell_count_eng]',
                                                    'value' => $options['cart_table_cell_count_eng']?$options['cart_table_cell_count_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'cart_table_cell_sum',
                                            'lang' => array(
                                                'title' => 'Колонка cумарно:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[cart_table_cell_sum]',
                                                    'value' => $options['cart_table_cell_sum']?$options['cart_table_cell_sum']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[cart_table_cell_sum_ua]',
                                                    'value' => $options['cart_table_cell_sum_ua']?$options['cart_table_cell_sum_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[cart_table_cell_sum_eng]',
                                                    'value' => $options['cart_table_cell_sum_eng']?$options['cart_table_cell_sum_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'cart_summary_title',
                                            'lang' => array(
                                                'title' => 'Заголовок Сумма в корзине:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[cart_summary_title]',
                                                    'value' => $options['cart_summary_title']?$options['cart_summary_title']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[cart_summary_title_ua]',
                                                    'value' => $options['cart_summary_title_ua']?$options['cart_summary_title_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[cart_summary_title_eng]',
                                                    'value' => $options['cart_summary_title_eng']?$options['cart_summary_title_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'cart_button_update_cart',
                                            'lang' => array(
                                                'title' => 'Кнопка Обновить:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[cart_button_update_cart]',
                                                    'value' => $options['cart_button_update_cart']?$options['cart_button_update_cart']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[cart_button_update_cart_ua]',
                                                    'value' => $options['cart_button_update_cart_ua']?$options['cart_button_update_cart_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[cart_button_update_cart_eng]',
                                                    'value' => $options['cart_button_update_cart_eng']?$options['cart_button_update_cart_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                    <td colspan="1">
                                        <?php
                                        $args = array(
                                            'id' => 'cart_button_process_to_checkout',
                                            'lang' => array(
                                                'title' => 'Кнопка Перейти к оформлению:',
                                                'ru' => array(
                                                    'name' => 'theme_settings[cart_button_process_to_checkout]',
                                                    'value' => $options['cart_button_process_to_checkout']?$options['cart_button_process_to_checkout']:''
                                                ),
                                                'ua' => array(
                                                    'name' => 'theme_settings[cart_button_process_to_checkout_ua]',
                                                    'value' => $options['cart_button_process_to_checkout_ua']?$options['cart_button_process_to_checkout_ua']:''
                                                ),
                                                'eng' => array(
                                                    'name' => 'theme_settings[cart_button_process_to_checkout_eng]',
                                                    'value' => $options['cart_button_process_to_checkout_eng']?$options['cart_button_process_to_checkout_eng']:''
                                                )
                                            )
                                        );
                                        echo view_lang_input($args);
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        </section>
                    </div>
                </div>
                <p><input name="submit" id="submit" class="button button-primary my-button-primary" value="Сохранить" type="submit"></p>
            </form>
        </div>
    </div>

<?php }