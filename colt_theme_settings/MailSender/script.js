//$=jQuery;
$(function () {
    function MailSender() {

        var data = {
            questionForm:'question_form',
            questionFormBtn:'question_form_sbm_btn',
            byOnOneClick:'by_one_click',
            byOnOneClickBtn:'by_one_click_sbm_btn'
        };
        var object = this;

        this.init = function () {
            $('#'+data.questionFormBtn).click(function (e) {
                e.preventDefault();
                object.sendAjaxQuestion();
            });
            $('#'+data.byOnOneClickBtn).click(function () {
                object.sendAjaxOneClick($('#one_click_btn_single_product').attr('data-id'));
            });
        };

        this.sendAjaxQuestion = function () {
            var ajax_array = new FormData($('#'+data.questionForm)[0]);
            ajax_array.append('action','mailsender');
            ajax_array.append('types','question');
            $.ajax({
                type: 'POST',
                url: ajax_url.url,
                processData: false,
                contentType: false,
                data: ajax_array,
                success: function(data){
                    console.log(data);
                    $('.close').click();
                    alert('Ваш вопрос успешно отправлен');
                },
                error: function () {
                    console.log('Ошибка');
                }
            });
        };
        this.sendAjaxOneClick = function (id) {
            var ajax_array = new FormData($('#'+data.byOnOneClick)[0]);
            ajax_array.append('action','mailsender');
            ajax_array.append('types','one_click');
            ajax_array.append('id',id);
            $.ajax({
                type: 'POST',
                url: ajax_url.url,
                processData: false,
                contentType: false,
                data: ajax_array,
                success: function(data){
                    console.log(data);
                    $('.close').click();
                    alert('Ваш вопрос успешно отправлен');
                },
                error: function () {
                    console.log('Ошибка');
                }
            });
        };
    }

    (new MailSender()).init();
});