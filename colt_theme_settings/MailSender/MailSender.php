<?php

/**
 * Разработал Максим Руденко
 * Вебстудия Penguin Studio
 * Сайт компании dp.pengstud.com
 * Дата: 16.06.2016
 */
class MailSender
{
    function __construct()
    {
        add_action( 'wp_enqueue_scripts', array($this, 'init_scripts') );

        add_action('wp_ajax_nopriv_mailsender', array($this, 'ajax'));
        add_action('wp_ajax_mailsender', array($this, 'ajax'));
    }
    public function init_scripts(){
        if ( ! did_action( 'wp_enqueue_media' ) ){ wp_enqueue_media(); }
        wp_enqueue_script( 'mailsender_script', get_template_directory_uri().'/colt_theme_settings/MailSender/script.js', array('jquery'), null, false );
        wp_localize_script('mailsender_script', 'ajax_url',
            array(
                'url' => admin_url('admin-ajax.php')
            )
        );
    }

    public function ajax(){
		
        $options = get_option( 'theme_settings' );
        $email = get_option('admin_email');
//        $email   = isset($options['contacts_email'])?$options['contacts_email']:get_option('admin_email');

		if(strcmp($_POST['types'], 'question')==0){
			$name = htmlspecialchars($_POST['name']);
            $phone = htmlspecialchars($_POST['phone']);
            $user_email = htmlspecialchars($_POST['email']);
            $sub = htmlspecialchars($_POST['sub']);

            $subject = "Пользователь задал вопрос";

            $message = "Имя: ".$name."<br>"
                ."Телефон: ".$phone."<br>"
                ."Email: ".$user_email
                ."Вопрос: ".$sub;

            @mail($email, $subject, $message);
        }
        if(strcmp($_POST['types'], 'one_click')==0){
            $id = $_POST['id'];
            $name = htmlspecialchars($_POST['name']);
            $phone = htmlspecialchars($_POST['phone']);
            $user_email = htmlspecialchars($_POST['email']);
            $sub = htmlspecialchars($_POST['sub']);

            $subject = "Покупка в один клик";
            $message = "Название товара: <a href='".get_permalink($id)."'>".get_the_title($id)."</a><br>"
                    ."Имя: ".$name."<br>"
                    ."Телефон: ".$phone."<br>"
                    ."Email: ".$user_email
                    ."Описание: ".$sub;

            @mail($email, $subject, $message);
        }
        wp_die();
    }

}
new MailSender();