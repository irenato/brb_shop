/**
 * Created by renato on 10.12.16.
 */

$(document).ready(function () {

    $('#exampleInputTel').mask('+38?(999) 999-99-99');

    function number(obj) {
        var rv_name = /^\+38.[0-9]{3}.\s[0-9]{3}\-[0-9]{2}(\-.{2})?$/;
        if (rv_name.test($(obj).val())) {
            $(obj).removeClass('error').addClass('success');
        } else {
            $(obj).removeClass('success').addClass('error');
        }
    }

    function emailValid(obj) {
        var rv_mail = /.+@.+\..+/i;

        if ($(obj).val() !== '' && rv_mail.test($(obj).val())) {
            $(obj).removeClass('error').addClass('success');
        } else {
            $(obj).removeClass('success').addClass('error');
        }
    }

    function notBeEmpty(obj) {
        if ($(obj).val().length != 0) {
            $(obj).removeClass('error').addClass('success');
        } else {
            $(obj).removeClass('success').addClass('error');
        }
    }

    $(document).on('click', '#form-summary button', function (e) {
        $('#summary-success').hide();
        $('#summary-error').hide();
        e.preventDefault();
        var username = $('#exampleInputName').val(),
            email = $('#exampleInputEmail').val(),
            city = $('#exampleInputCity').val(),
            phone = $('#exampleInputTel').val(),
            link = $('#exampleInputSite').val(),
            summary = $('#exampleInputabout').val();

        var current_form = $('#form-summary');
        $(current_form).find('input').each(function () {
            var name = $(this).attr('name'),
                val = $(this).val();

            switch (name) {
                case 'your-email':
                    emailValid(this);
                    break;

                case 'your-telephone':
                    number(this);
                    break;

                default:
                    notBeEmpty(this);
                    break;
            }
        })

        if ($(current_form).find('.error').length < 1) {
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'addSummary',
                    'username': username,
                    'email': email,
                    'phone': phone,
                    'link': link,
                    'city': city,
                    'summary': summary,
                },
                success: summaryAdded,
            });
        }
    })

    function summaryAdded(result) {
        if(result == 'done!'){
            $('#form-summary input').val('');
            $('#form-summary textarea').val('');
            $('#summary-success').show();
        }else
            $('#summary-error').show();
    }

})
