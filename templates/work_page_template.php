<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 10.12.16
 * Time: 16:59
 */

/**
 * Template name: Work
 */

get_header();
?>

    <!--работа у нас-->
    <div class="container ">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="main-blocks about-us workbarbershop col-md-12">
                    <h2 class="main-blocks__title">РАБОТА В mr.colt BARBERSHOP</h2>
                    <p>Заполни форму и стань частью большой семьи свободных барберов.</p>
                    <form id="form-summary" action="work_submit" method="get" accept-charset="utf-8">
                        <div class="form-group1">
                            <label for="exampleInputName">Имя* (обязательно)</label>
                            <input type="text" name="your-name" class="form-control" id="exampleInputName">
                        </div>
                        <div class="form-group1">
                            <label for="exampleInputCity">Город* (обязательно)</label>
                            <input type="text" name="your-city" class="form-control" id="exampleInputCity">
                        </div>
                        <div class="form-group1">
                            <label for="exampleInputEmail">Email* (обязательно)</label>
                            <input type="email" name="your-email" class="form-control" id="exampleInputEmail">
                        </div>
                        <div class="form-group1">
                            <label for="exampleInputTel">Телефон* (обязательно)</label>
                            <input type="tel" name="your-telephone" class="form-control" id="exampleInputTel" placeholder="+380">
                        </div>
                        <div class="form-group1">
                            <label for="exampleInputSite">Профиль в Социальных сетях</label>
                            <input type="text" name="your-socialsite" class="form-control" id="exampleInputSite" placeholder="Facebook/Vkontakte">
                        </div>
                        <div class="form-group1">
                            <label for="exampleInputabout">О себе / уровень / квалификация</label>
                            <textarea name="your-aboutyourself" class="form-control" id="exampleInputabout"></textarea>
                        </div>
                        <span id="summary-success" class="text-success">Благодарим. Мы обязательно свяжемся с Вами!</span>
                        <span id="summary-error" class="text-danger">Возникла ошибка. Попробуйте позже.</span>
                        <button type="submit" class="generalButton">Отправить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php

get_footer();
