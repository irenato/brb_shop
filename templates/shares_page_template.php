<?php
/*
Template Name: Шаблон страницы акции
*/
/**
 * Create by: BitLab
 * Date: 08.11.2016
 * Файл отвечает за вывод страницы акции
 */
$actions_obj = PTP::getActions();
$actions_arr = array();
foreach ($actions_obj as $action) {
    array_push($actions_arr, $action->p2p_from);
}


?>
<?php get_header(); ?>
    <!--акции-->
    <div class="container promo-container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="main-blocks about-us col-md-12">
                    <h2 class="main-blocks__title">акции</h2>
                    <div class="promo">
                        <ul class="card-box">
                        <?php
                        $actions = new WP_Query(array(
                            'post_type' => 'promo',
                            'post__in' => $actions_arr,
                        ));
                        ?>
                        <?php if ($actions->have_posts()) : while ($actions->have_posts()) :
                        $actions->the_post(); ?>
                            <li>
                                <div class="card-box__wrapp">
                                    <img src="<?= get_field('action_image') ?>">
                                    <div class="bottom-menu">
                                        <h4 class="bottom-menu__name"><?= get_the_title() ?></h4>
                                        <p class="bottom-menu__name-desc"><?= wp_trim_words(get_the_content(), 12, '...'); ?></p>
                                        <a class="bottom-menu__link" href="<?= get_the_permalink(); ?>">Посмотреть</a>
                                    </div>
                                </div>
                            </li>
                            <?php endwhile; ?>
                            <?php endif; ?>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hidden-sm hidden-xs separator-line"></div>
<?php get_footer(); ?>