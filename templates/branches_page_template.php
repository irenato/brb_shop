<?php
/*
Template Name: Шаблон страницы выбор филиала
*/
/**
 * User: BitLab
 * Date: 07.11.2016
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= wp_title() ?></title>
    <meta name="robots" content="noindex">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= THEMROOT ?>/css/style.css">
    <link rel="stylesheet" href="<?= THEMROOT ?>/css/temp.css">
    <link rel="stylesheet" href="<?= THEMROOT ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= THEMROOT ?>/css/lightslider.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script>

        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-74992501-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
<div class="choos-address">
<!--    <a href="#" class="buttonOnline">Онлайн запись</a>-->
    <div class="main-blocks about-us col-md-12">
        <h2 class="main-blocks__title">Выбор филиала</h2>
        <ul class="card-box">
            <?php $args = array(
                'post_type' => 'brb_filial',
                'orderby' => 'ID',
                'order' => 'ASC'); ?>
            <?php $post_filial = new WP_query($args); ?>
            <?php $count = 1; ?>
            <?php while ($post_filial->have_posts()) : $post_filial->the_post(); ?>
                <li>
                    <div class="card-box__wrapp">
                        <img src="<?= get_field('filial_image'); ?>">
                        <div class="bottom-menu">
                            <a class="bottom-menu__link" href="<?= get_home_url() . '?branch=' . get_the_ID() ?>">
                                <h4 class="bottom-menu__name"><?= get_the_title(); ?></h4>
                                <p class="bottom-menu__name-desc"><?= get_the_content(); ?></p>
                            </a>
                        </div>
                    </div>
                </li>
				<?php $count++; ?>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        </ul>
    </div>
</div>

<!-- bower:js -->
<!--<script type="text/javascript" src="https://w15352.yclients.com/widgetJS" charset="UTF-8"></script>-->
<script src="<?= THEMROOT ?>/js/jquery.min.js"></script>
<script src="<?= THEMROOT ?>/js/lightslider.js"></script>
<script src="<?= THEMROOT ?>/js/common.js"></script>
<script src="<?= THEMROOT ?>/js/bootstrap.min.js"></script>
<!-- endbower -->
</body>
</html>

