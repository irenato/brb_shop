<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 10.12.16
 * Time: 16:56
 */

/**
 * Template name: FAQ
 */

get_header();

$questions = get_field('block_faq');
?>

    <div class="container ">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="main-blocks about-us faqbarbershop col-md-12">
                    <h2 class="main-blocks__title">FAQ</h2>
                    <ol class="faqList">
                        <?php if ($questions): ?>
                            <?php foreach ($questions as $question): ?>
                                <li>
                                    <strong><?= $question['question'] ?></strong>
                                    <p><?= $question['ask'] ?></p>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ol>
                    <!-- Button trigger modal -->
                    <button class="generalButton buttonWidth" data-toggle="modal" data-target="#myModal">
                        Задать вопрос
                    </button>
                    <!-- Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Задайте вопрос</h4>
                                </div>
                                <div class="modal-body">
                                    <form id="question_form" action="" method="get" accept-charset="utf-8">
                                        <div class="form-group1">
                                            <label for="exampleInputName">Имя</label>
                                            <input type="text" name="name" class="form-control"
                                                   id="exampleInputName">
                                        </div>
                                        <div class="form-group1">
                                            <label for="exampleInputEmail">Email</label>
                                            <input type="email" name="email" class="form-control"
                                                   id="exampleInputEmail">
                                        </div>
                                        <div class="form-group1">
                                            <label for="exampleInputTel">Телефон</label>
                                            <input type="tel" name="phone" class="form-control"
                                                   id="exampleInputTel" placeholder="+380">
                                        </div>
                                        <div class="form-group1">
                                            <label for="exampleInputabout">Вопрос</label>
                                            <textarea name="question" class="form-control" name="sub"
                                                      id="exampleInputabout"></textarea>
                                        </div>
                                        <button type="submit" id="question_form_sbm_btn" class="generalButton">Отправить</button>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php

get_footer();
