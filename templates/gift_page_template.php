<?php
/*
Template Name: Шаблон страницы подарочный сертификат
*/
?>
<?php get_header(); ?>

<!--подарочный сертификат-->
<div class="container container-gift">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="main-blocks about-us col-md-12">
                <h2 class="main-blocks__title">подарочный сертификат</h2>
                <div class="gift">
                    <?php $img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full') ?>
                    <img src="<?= $img[0] ?>">
                    <?php $post_id_2718 = get_post(2718); ?>
                    <p><?= $post_id_2718->post_content ?></p>
                    <div class="selectGift">
                        <?php $options = get_option('services'); ?>

                        <?php if (isset($options['service'])):
                        $array = $options['service'];
                        //print_r($array);
                        $array_count = count($options['service']);
                        $array_count_1 = (int)($array_count / 2) + $array_count % 2;
                        $array_count_2 = $array_count - $array_count_1;
                        $services_slugs = filter_services_id_by_filial();
                        ?>

                        <form>
                            <div class="form-group">
                                <label for="exampleSelect1">Выберите услугу:</label>
<!--                                <label for="exampleSelect1">Подарочный сертификат на сумму:</label>-->
                                <select class="form-control" id="exampleSelect1">
                                    <?php for ($i = 0; $i < $array_count_1; $i++):
                                        $service = $array[$i];
                                        $title = isset($service['title']) ? $service['title'] : '';
                                        $price = isset($service['price']) ? $service['price'] : '';
                                        $url = isset($service['url']) ? $service['url'] : '#';
                                        $slug = create_slug_from_services_url($url);
                                        ?>
                                        <?php if (in_array($slug, $services_slugs)) : ?>
                                    <option><?= esc_attr($title) ?></option>
                                        <?php endif; ?>
                            <?php endfor; ?>
<!--                                    <option>200 грн</option>-->
<!--                                    <option>300 грн</option>-->
<!--                                    <option>400 грн</option>-->
<!--                                    <option>500 грн</option>-->
                                </select>
                                <a href="#" data-toggle="modal" data-target="#giftModal" id="openGiftModal">Оформить</a>
                            </div>
                        </form>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="emb-responsive">
                    <?php $youtube_link = get_post_meta(2718, 'youtube_link'); ?>
                    <iframe width="100%" height="480" src="<?= $youtube_link[0] ?>" frameborder="0"
                            allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container commets-container">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="main-blocks about-us col-md-12">
                <h2 class="main-blocks__title">отзывы</h2>
                <div class="reviewsN reviewsNew">
                    <?php
                    global $post;
                    woocommerce_view_comments($post);
                    ?>
                </div>
                <div class="comments">
                    <?php woocommerce_view_form(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
