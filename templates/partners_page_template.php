<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 10.12.16
 * Time: 16:58
 */

/**
 * Template name: Partners
 */

get_header();
$partners = get_post(2784);
$phones = get_field('contact_phones');

?>

    <!--партнерам-->
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="main-blocks about-us partners col-md-12">
                    <h2 class="main-blocks__title"><?= get_the_title() ?></h2>
                    <p><?= $partners->post_content ?></p>
                    <?php if ($phones): ?>
                        <?php foreach ($phones as $phone): ?>
                            <p class="telPartners"><?= $phone['phone'] ?></p>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

<?php

get_footer();
