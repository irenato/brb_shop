<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 13.12.16
 * Time: 20:03
 */

/**
 * Template name: About block
 */

get_header();

?>

<?php get_template_part('content/main_page/about_us'); ?>

    <div class="hidden-sm hidden-xs separator-line"></div>

<?php

get_footer();
